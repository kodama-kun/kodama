﻿/*
 
 Kodama
 
 Released under WTFPL
 Do whatever you want with this. If you're going to use it to build nuclear missiles, be my guest.

 Includes parts of MonoTorrent library by Alan McGovern https://github.com/mono/monotorrent
 
 Requires Microsoft SQL Server Compact Edition https://www.nuget.org/packages/Microsoft.SqlServer.Compact
 
 */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
//using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Kodama
{
    public partial class MainWindow : Window
    {
    // TODO: config for baka regex
    // maybe some day.


        private BackgroundWorker HummingbirdWorker;

        public MainWindow()
        {
            InitializeComponent();

            

            UIHelper.uiDispatcher = System.Windows.Threading.Dispatcher.CurrentDispatcher;
            Settings.Load();
            Watchlist.Load();


            Hummingbird.Startup();
            BakaDB.Startup();
            BakaDB.worker.WorkerReportsProgress = true;
            BakaDB.worker.ProgressChanged += BakaWorker_ProgressChanged;
            BakaDB.worker.RunWorkerCompleted += BakaWorker_RunWorkerCompleted;


            HummingbirdWorker = new BackgroundWorker();
            HummingbirdWorker.DoWork += HummingbirdWorker_DoWork;
            HummingbirdWorker.WorkerReportsProgress = true;
            HummingbirdWorker.ProgressChanged += HummingbirdWorker_ProgressChanged;
            HummingbirdWorker.RunWorkerCompleted += HummingbirdWorker_RunWorkerCompleted;


            UIHelper.TabList = new ObservableCollection<NyaaTab>();

            DownloadListView.ItemsSource = TorrentView.TorrentList;

            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(DownloadListView.ItemsSource);
            PropertyGroupDescription groupDescription = new PropertyGroupDescription("Group");
            view.GroupDescriptions.Add(groupDescription);


            NyaaTabControl.ItemsSource = UIHelper.TabList;
            FolderTreeview.ItemsSource = UIHelper.FolderList;
            FooItemsControl.ItemsSource = UIHelper.FooList;
            SGfranchiselist.ItemsSource = UIHelper.FranchiseList;

            SettingsWatchlistGrid.ItemsSource = Watchlist.Entries;
           
            SettingsPortTextBox.DataContext = UIHelper.SettingsHelper;
            SettingsReleaseGroupTextBox.DataContext = UIHelper.SettingsHelper;

            ButtonSelectCompletedFolder.Content = Settings.All.CompletedPath;

            SideGrid.Visibility = Visibility.Collapsed;

            GenerateTree();


            TorrentFunctions.StartEngine();


            LoadSliderLimits();

         //   LoadSettingsFields();

           

            BakaSearch.RebuildTitleCache();


            TorrentFunctions.engine.StatsUpdate += engine_StatsUpdate;



            TorrentView.ScanTorrentDirectory();

            BuildVersionLabel.Text = Tools.GetVersionString();

      
        }




        void engine_StatsUpdate(object sender, MonoTorrent.Client.StatsUpdateEventArgs e)
        {
           
            string stats = (TorrentFunctions.TotalDownloadSpeed) + "k down, " + (TorrentFunctions.TotalUploadSpeed) + "k up";



            UIHelper.uiDispatcher.BeginInvoke(new Action(() =>
            {
                StatusSpeedLabel.Content = stats;
                StatusDiskSpaceLabel.Content = DiskSpace.GetDiskSpace() + " GB free";
            }));

        }



        private void buttonLoadDB_Click(object sender, RoutedEventArgs e)
        {
            HummingbirdWorker.RunWorkerAsync();
            buttonLoadDB.IsEnabled = false;
            buttonLoadDB.Content = "updating...";
            HummingbirdProgress.Visibility = System.Windows.Visibility.Visible;
        }




        private void HummingbirdWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            int lastpage = Hummingbird.getLastPage();

            UIHelper.uiDispatcher.BeginInvoke(new Action(() =>
            {
                HummingbirdProgress.Maximum = lastpage;
            }));



            for (int i = 1; i < lastpage + 1; i++)
            {
                Hummingbird.parseSinglePage(i);
                HummingbirdWorker.ReportProgress(i);
            }
        }

        private void HummingbirdWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            buttonLoadDB.IsEnabled = true;
            buttonLoadDB.Content = "Update database";
            Settings.All.LastChartsUpdate = DateTime.UtcNow;
        }

        void HummingbirdWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            GenerateTree();
            UpdateDBCount(true);
            HummingbirdProgress.Value = e.ProgressPercentage;
        }








        private void buttonLoadTree_Click(object sender, RoutedEventArgs e)
        {

            GenerateTree();

        }




        private async void treeview2_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (FolderTreeview.SelectedItem == null) return;

            Folder item = (Folder)(FolderTreeview.SelectedItem);

            if (item != null && item.Folders.Count == 0)
            {
                Debug.WriteLine(item.Label + " " + item.start + " " + item.end);
                //  var dtable = hummingbird.edb.DisplayTableDateBetween("anime", "*", "started_airing", item.start, item.end);
                //  datagrid1.DataContext = dtable.DefaultView;

                SideGrid.Visibility = Visibility.Collapsed;
                await DisplayChart(item.start, item.end);
            }

        }




        public void GenerateTree()
        {
            UIHelper.FolderList.Clear();

            var dtable = Hummingbird.edb.DisplayTable("anime ORDER BY started_airing DESC", "TOP 1 started_airing");
            DateTime latest = new DateTime(0);
            try
            {
                latest = dtable.Rows[0].Field<DateTime>(0);
            }
            catch { }

            Debug.WriteLine("latest date: " + latest);

            //  uitypes.FolderList.Add(new Folder { Label = "Unknown Date" });

            var fpre70 = new Folder { Label = "Before 1970" };
            for (int i = 1920; i <= 1960; i += 10)
            {
                if (latest.Year < i) break;
                fpre70.Folders.Add(new Folder { Label = (i).ToString(), start = new DateTime((i), 1, 1), end = new DateTime((i + 10), 1, 1), });
            }
            if (latest.Year >= 1920) UIHelper.FolderList.Add(fpre70);


            var f70 = new Folder { Label = "1970s" };
            for (int i = 1970; i < 1980; i++)
            {
                if (latest.Year < i) break;
                f70.Folders.Add(new Folder { Label = i.ToString(), start = new DateTime(i, 1, 1), end = new DateTime(i + 1, 1, 1), });
            }
            if (latest.Year >= 1970) UIHelper.FolderList.Add(f70);


            var f80 = new Folder { Label = "1980s" };
            for (int i = 1980; i < 1990; i++)
            {
                if (latest.Year < i) break;
                f80.Folders.Add(new Folder { Label = i.ToString(), start = new DateTime(i, 1, 1), end = new DateTime(i + 1, 1, 1), });
            }
            if (latest.Year >= 1980) UIHelper.FolderList.Add(f80);



            Folder fp = null;
            for (int j = 1990; j < 2020; j++)
            {
                if (latest.Year < j) break;

                if (j % 10 == 0)
                {
                    if (fp != null) UIHelper.FolderList.Add(fp);
                    fp = new Folder { Label = j + "s" };
                }

                var fs = new Folder { Label = j.ToString() };
                fs.Folders.Add(new Folder { Label = "Winter " + j.ToString(), start = new DateTime(j, 1, 1), end = new DateTime(j, 4, 1) });
                fs.Folders.Add(new Folder { Label = "Spring " + j.ToString(), start = new DateTime(j, 4, 1), end = new DateTime(j, 7, 1) });
                fs.Folders.Add(new Folder { Label = "Summer " + j.ToString(), start = new DateTime(j, 7, 1), end = new DateTime(j, 10, 1) });
                fs.Folders.Add(new Folder { Label = "Autumn " + j.ToString(), start = new DateTime(j, 10, 1), end = new DateTime(j + 1, 1, 1) });

                fp.Folders.Add(fs);
            }
            UIHelper.FolderList.Add(fp);
        }



        ObservableCollection<ChartEntry> getBarList(DataTable dtable)
        {
            var tBarList = new ObservableCollection<ChartEntry>();

            for (int i = 0; i < dtable.Rows.Count; i++)
            {
                tBarList.Add(new ChartEntry
                {
                    Name = dtable.Rows[i].Field<String>(0),
                    _url = dtable.Rows[i].Field<String>(1),
                    aid = dtable.Rows[i].Field<String>(2)
                });
            }
            return tBarList;
        }



        async Task DisplayChart(DateTime dfrom, DateTime dto)
        {
            UIHelper.FooList.Clear();

            UIHelper.FooList.Add(new ChartView
            {
                Name = "TV Series",
                ChartEntryList = getBarList(
                    await Hummingbird.edb.DisplayTableCMDAsync("anime", "SELECT canonical_title, poster_image, url FROM anime WHERE started_airing >= @p1 AND started_airing < @p2 AND show_type =@p3 ORDER BY started_airing", dfrom, dto, "TV"))
            });


            if (Settings.All.ShowMoviesAndSpecials == true)
            {
                UIHelper.FooList.Add(new ChartView
                {
                    Name = "Movies",
                    ChartEntryList = getBarList(
                        await Hummingbird.edb.DisplayTableCMDAsync("anime", "SELECT canonical_title, poster_image, url FROM anime WHERE started_airing >= @p1 AND started_airing < @p2 AND show_type =@p3 ORDER BY started_airing", dfrom, dto, "Movie"))
                });


                UIHelper.FooList.Add(new ChartView
                {
                    Name = "OVAs and Specials",
                    ChartEntryList = getBarList(
                        await Hummingbird.edb.DisplayTableCMDAsync("anime", "SELECT canonical_title, poster_image, url FROM anime WHERE started_airing >= @p1 AND started_airing < @p2 AND (show_type =@p3 OR show_type=@p4 OR show_type=@p5)  ORDER BY started_airing", dfrom, dto, "Special", "OVA", "ONA"))
                });
            }
        }








        private async void StackPanel_Initialized(object sender, EventArgs e)
        {
            string _url = ((StackPanel)sender).Tag.ToString();
            var localfile = "dbimages\\" + _url.GetHashCode().ToString() + ".jpg";

            try
            {
                if (File.Exists(localfile) == false)
                {
                    try
                    {
                        await Tools.DownloadAndCheckImage(_url.Replace("/large/", "/medium/"), localfile);
                    }
                    catch
                    {
                        // downloading hires version would go here but... "cannot await in catch" - hello, c#5
                    }

                    if (File.Exists(localfile) == false) await Tools.DownloadAndResizeImage(_url, localfile, 140, 200, true);
                }
                ((StackPanel)sender).Background = new ImageBrush(new BitmapImage(new Uri(System.IO.Path.GetFullPath(localfile))));
            }
            catch { }
        }

        private void Button_Click_ChartEntry(object sender, RoutedEventArgs e)
        {
            SideGrid.Visibility = Visibility.Visible;

            string aid = ((Button)sender).Tag.ToString();

            changeSGView(aid);
        }


        void changeSGView(string aid)
        {
            var dtable = Hummingbird.edb.DisplayTableCMD("anime", "SELECT TOP 1 canonical_title, english_title, poster_image, synopsis, started_airing, franchise, romaji_title, alternate_title FROM anime WHERE url = @p1", aid);


            SGtitle.Text = dtable.Rows[0].Field<String>(0);
            SGsynopsis.Tag = dtable.Rows[0].Field<String>(3);
            SGsynopsis.Text = Tools.ShortenString(dtable.Rows[0].Field<String>(3), "... (read more)", 200);

            string _url = dtable.Rows[0].Field<String>(2);
            var localfile = "dbimages\\" + _url.GetHashCode().ToString() + ".jpg";
            if (File.Exists(localfile))
            {
                var bgimg = new BitmapImage(new Uri(System.IO.Path.GetFullPath(localfile)));
                SGimagelowres.Source = bgimg;
            }
            SGimage.Source = new BitmapImage(new Uri(_url));

            SGairdate.Text = dtable.Rows[0].Field<DateTime>(4).ToString("MMM dd, yyyy");



            string advancedTag = "";

            HashSet<string> alltitles = new HashSet<string>();
            alltitles.Add(aid.Replace("-", " ").ToLower());
            try
            {
                alltitles.Add(dtable.Rows[0].Field<String>(0).ToLower() + ""); //what about DBNULL?
                alltitles.Add(dtable.Rows[0].Field<String>(1).ToLower() + "");
                alltitles.Add(dtable.Rows[0].Field<String>(6).ToLower() + "");
                alltitles.Add(dtable.Rows[0].Field<String>(7).ToLower() + "");
            }
            catch (Exception ex) { /*let's not try to be too clever here*/  }
            foreach (string str in alltitles)
            {
                if (str.Length > 3) advancedTag += str + "\n";
            }
            advancedTag = advancedTag.TrimEnd('\n');

            SGButtonNyaaSearchAdvancedText.Text = "Search for " + dtable.Rows[0].Field<String>(0);
            SGButtonNyaaSearchAdvanced.Tag = advancedTag;


            var franchiseid = dtable.Rows[0].Field<String>(5);
            UIHelper.FranchiseList.Clear();
            SGFranchiseBorder.Visibility = System.Windows.Visibility.Collapsed;
            if (franchiseid != null & franchiseid.Length > 1)
            {
                var franchisedtable = Hummingbird.edb.DisplayTableCMD("anime", "SELECT canonical_title, started_airing, url, show_type  FROM anime WHERE franchise = @p1 AND url <> @p2", franchiseid, aid);

                for (int i = 0; i < franchisedtable.Rows.Count; i++)
                {
                    string flabel = "(couldn't load title)";

                    try
                    {
                        var dateyear = "(" + franchisedtable.Rows[i].Field<DateTime>(1).ToString("yyyy") + ", ";
                        dateyear += franchisedtable.Rows[i].Field<String>(3) + ") ";
                        flabel = dateyear + franchisedtable.Rows[i].Field<String>(0);
                    }
                    catch { }



                    UIHelper.FranchiseList.Add(new FranchiseButton { Label = flabel, Url = franchisedtable.Rows[i].Field<String>(2) });
                }


                if (UIHelper.FranchiseList.Count > 0) SGFranchiseBorder.Visibility = System.Windows.Visibility.Visible;
            }
        }





        private void SGButtonNyaaSearch_Click(object sender, RoutedEventArgs e)
        {
            string searchterm = ((Button)sender).Tag.ToString().Replace("-", " ");
            UIHelper.TabList.Add(new NyaaTab("", searchterm, "", "", true));
        }

        private void SGfranchiselist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (SGfranchiselist.SelectedItem != null)
            {
                var url = ((FranchiseButton)SGfranchiselist.SelectedItem).Url;
                changeSGView(url);
            }
        }

        private void FolderTreeviewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                var tvi = sender as TreeViewItem;
                var nyaaitem = tvi.DataContext as NyaaResult;
                if (nyaaitem.Item != null)
                {
                    nyaaitem.IsChecked = !nyaaitem.IsChecked;
                }
            }
            catch { }
        }

        private void TextBlock_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Middle && e.ButtonState == MouseButtonState.Released)
            {
                var header = sender as Border;
                var thetab = (NyaaTab)header.Tag;
                UIHelper.TabList.Remove(thetab);
            }
        }

        private void BakaButtonLoadDB_Click(object sender, RoutedEventArgs e)
        {
            BakaDB.StartGeneratingDB();
            BakaProgress.Visibility = System.Windows.Visibility.Visible;
            BakaButtonLoadDB.IsEnabled = false;
            BakaButtonLoadDB.Content = "updating...";
        }


        void BakaWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            BakaProgress.Value = e.ProgressPercentage;
            UpdateDBCount(true);

        }


        void BakaWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BakaButtonLoadDB.IsEnabled = true;
            BakaButtonLoadDB.Content = "Update database";
            Settings.All.LastBakaUpdate = DateTime.UtcNow;
            BakaSearch.RebuildTitleCache();
        }
    



        List<SearchNyaa.NyaaItem> TraverseNyaaTree(ObservableCollection<NyaaResult> treebase)
        {
            var result = new List<SearchNyaa.NyaaItem>();

            foreach (var childnyaa in treebase)
            {
                if (childnyaa.Item != null && childnyaa.IsChecked == true)
                {
                    result.Add(childnyaa.Item);
                    childnyaa.IsChecked = false;
                }

                foreach (var childresultitem in TraverseNyaaTree(childnyaa.children))
                {
                    result.Add(childresultitem);
                }
            }

            return result;
        }



        private void StatusBar_Button_Click_SwitchTab(object sender, RoutedEventArgs e)
        {
            var targetTab = ((Button)sender).Tag;
            MainTabWindow.SelectedIndex = Int32.Parse(targetTab.ToString());

            UpdateDBCount(false);
        }



        private void UI_Closed(object sender, EventArgs e)
        {
            Settings.Save();
            Watchlist.Save();
            TorrentView.Shutdown();
            TorrentFunctions.StopEngine();
            //      Settings.Save();
        }


        private void MenuItem_Click_Pause(object sender, RoutedEventArgs e)
        {
            foreach (TorrentListEntry sitem in DownloadListView.SelectedItems)
            {
                sitem.MenuPause();
            }


        }

        private void MenuItem_Click_Start(object sender, RoutedEventArgs e)
        {
            foreach (TorrentListEntry sitem in DownloadListView.SelectedItems)
            {
                sitem.MenuStart();
            }
        }

        private void MenuItem_Click_Stop(object sender, RoutedEventArgs e) //obsolete
        {
            foreach (TorrentListEntry sitem in DownloadListView.SelectedItems)
            {
                sitem.MenuStop();
            }
        }

        private void MenuItem_Click_Remove(object sender, RoutedEventArgs e)
        {
            if (DownloadListView.SelectedItems.Count == 0) return;

            MessageBoxResult result = MessageBox.Show("Do you want to delete the .torrent file\nand downloaded data of the selected torrents?", "Are you sure?", MessageBoxButton.OKCancel);
            if (result == MessageBoxResult.OK)
            {
                foreach (TorrentListEntry sitem in DownloadListView.SelectedItems)
                {
                    sitem.MenuDelete();
                }
            }
        }

        void LoadSliderLimits()
        {
            var tempdown = Settings.All.DownLimit;
            var tempup = Settings.All.UpLimit;
            var tempactive = Settings.All.MaxActiveTorrents;

            PopupLimitSliderUp.Value = 1;
            PopupLimitSliderDown.Value = 1;
            PopupLimitSliderActive.Value = 1;

            Settings.All.DownLimit = tempdown;
            Settings.All.UpLimit = tempup;
            Settings.All.MaxActiveTorrents = tempactive;

            if (Settings.All.DownLimit != 0)
            {
                double reverseDownLimit = (double)(Settings.All.DownLimit - 1) / (double)Settings.All.ConnectionDownSpeed;
                reverseDownLimit = Math.Sqrt(reverseDownLimit);
                PopupLimitSliderDown.Value = reverseDownLimit;
            }

            if (Settings.All.UpLimit != 0)
            {
                double reverseUpLimit = (double)(Settings.All.UpLimit - 1) / (double)Settings.All.ConnectionUpSpeed;
                reverseUpLimit = Math.Sqrt(reverseUpLimit);
                PopupLimitSliderUp.Value = reverseUpLimit;
            }



            double reverseActive = (double)(Settings.All.MaxActiveTorrents - 1) / (double)Settings.All.MaxActiveLimit;
            reverseActive = Math.Sqrt(reverseActive);
            Debug.WriteLine(reverseActive);
            PopupLimitSliderActive.Value = reverseActive;
        }



        private void Slider_ValueChanged_Download(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var downlimit = ((Slider)sender).Value;
            if (downlimit < 1.0)
            {
                downlimit = Settings.All.ConnectionDownSpeed * (downlimit * downlimit);
                downlimit = Math.Ceiling(downlimit + 0.0001);

                Settings.All.DownLimit = (int)(downlimit);
                // TorrentFunctions.engine.Settings.GlobalMaxDownloadSpeed = ((int)(downlimit) * 1024);
                PopupLimitLabelDown.Content = "Download limit: " + downlimit.ToString() + " K/s";
            }
            else
            {
                Settings.All.DownLimit = 0;
                PopupLimitLabelDown.Content = "No download limit";
                // TorrentFunctions.engine.Settings.GlobalMaxDownloadSpeed = 0;
            }
        }

        private void Slider_ValueChanged_Upload(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            var uplimit = ((Slider)sender).Value;
            if (uplimit < 1.0)
            {
                uplimit = Settings.All.ConnectionUpSpeed * (uplimit * uplimit);
                uplimit = Math.Ceiling(uplimit + 0.0001);

                Settings.All.UpLimit = (int)uplimit;
                //TorrentFunctions.engine.Settings.GlobalMaxUploadSpeed = ((int)(uplimit) * 1024);
                PopupLimitLabelUp.Content = "Upload limit: " + uplimit.ToString() + " K/s";
            }
            else
            {
                Settings.All.UpLimit = 0;
                PopupLimitLabelUp.Content = "No upload limit";
                //TorrentFunctions.engine.Settings.GlobalMaxUploadSpeed = 0;
            }
        }


        private void PopupLimitSliderActive_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                var active = ((Slider)sender).Value;

                active = Settings.All.MaxActiveLimit * (active * active);
                active = Math.Ceiling(active + 0.0001);

                Settings.All.MaxActiveTorrents = (int)active;
                PopupLimitLabelActive.Content = "Active torrents: " + Settings.All.MaxActiveTorrents;
            }
            catch { }
        }






        private void Button_Click_SearchFromSearch(object sender, RoutedEventArgs e)
        {
            //var ntab = NyaaTabControl.SelectedItem as NyaaTab;

            var searchterm = SearchExternalTextBox.Text;
            if (searchterm != "")
            {
                var releasegroup = "";
                /*  if (SearchSelectReleaseGroup.SelectedIndex > 0)
                  {
                      releasegroup = SearchSelectReleaseGroup.SelectedItem.ToString();
                  }*/
                var quality = "";

                UIHelper.TabList.Add(new NyaaTab(searchterm, searchterm, releasegroup, quality, true));
                NyaaTabControl.SelectedIndex = UIHelper.TabList.Count - 1;
            }
        }


        private void NyaaTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (NyaaTabControl.SelectedItem == null) 
            {
                SearchButtonPanel.Visibility = System.Windows.Visibility.Collapsed;
                return;
            }
            var ntab = NyaaTabControl.SelectedItem as NyaaTab;
            SearchExternalTextBox.Text = ntab.workerArguments;
            SearchButtonPanel.Visibility = System.Windows.Visibility.Visible;
        }

        private void Button_Click_DownloadSelectedTorrents(object sender, EventArgs e)
        {
            if (NyaaTabControl.SelectedItem == null) return;
            var ntab = NyaaTabControl.SelectedItem as NyaaTab;
            if (ntab == null) return;

            ntab.ResultList = "Looking for items...";

            if (Directory.Exists(Settings.All.TorrentPath) == false) { Directory.CreateDirectory(Settings.All.TorrentPath); }

            var allnyaaitems = TraverseNyaaTree(ntab.TreeBase);

            foreach (var oneNyaa in allnyaaitems)
            {
                try
                {
                    TorrentView.DownloadTorrentInBackground(oneNyaa);
                }
                catch { }
            }
        }

        private void SGsynopsis_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            SGsynopsis.Text = SGsynopsis.Tag as string;
        }









        private DateTime lastDBCountUpdate = new DateTime(1900,1,1);
        void UpdateDBCount(bool showCount)
        {
            if (showCount==false && (DateTime.UtcNow - lastDBCountUpdate).TotalHours < 12) return;
            lastDBCountUpdate = DateTime.UtcNow;

            var count = Hummingbird.edb.GetTableCount("anime");
            if (count < 1)
            {
                ChartsExpander.Header = "Charts DB needs update!";
            }
            else
            {
                if (showCount == true) ChartsExpander.Header = "Charts DB (" + count + " entries)";
                else
                {
                    var days = (DateTime.UtcNow - Settings.All.LastChartsUpdate).TotalDays;
                    ChartsExpander.Header = "Charts DB (" + (int)days + " days old)";
                }
            }



            count = BakaDB.edb.GetTableCount("baka");
            if (count < 1)
            {
                BakaExpander.Header = "Baka DB needs update!";
            }
            else
            {
                if (showCount == true) BakaExpander.Header = "Baka DB (" + count + " entries)";
                else
                {
                    var days = (DateTime.UtcNow - Settings.All.LastBakaUpdate).TotalDays;
                    BakaExpander.Header = "Baka DB (" + (int)days + " days old)";
                }
            }
        }


      


        private void MenuItem_Click_SearchFromDownloads(object sender, RoutedEventArgs e)
        {
            var searchterms = new HashSet<string>();
            foreach (TorrentListEntry sitem in DownloadListView.SelectedItems)
            {
                try { searchterms.Add(sitem.Group); }
                catch { }
            }
            if (searchterms.Count > 0)
            {
                string searchqueries = "";
                foreach (var s in searchterms)
                {
                    searchqueries += s + "\n";
                }
                searchqueries = searchqueries.Trim('\n');
                UIHelper.TabList.Add(new NyaaTab("", searchqueries, "", "", true));
            }
        }



        private void Button_Click_ChooseFolderCompleted(object sender, RoutedEventArgs e)
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ((Button)sender).Content = dialog.SelectedPath;
                Settings.All.CompletedPath = dialog.SelectedPath;
            }
        }



        void LoadSettingsFields()
        {

            //  SettingsPortTextBox.Text = Settings.All.ListenPort.ToString();
            //  SettingsDownLimitTextBox.Text = Settings.All.ConnectionDownSpeed.ToString();
            //  SettingsUpLimitTextBox.Text = Settings.All.ConnectionUpSpeed.ToString();

            //  LoadReleaseGroupList();
        }

        private void TextBox_VerifyNumerical(object sender, TextCompositionEventArgs e)
        {
            try
            {
                var text = ((TextBox)sender).Text;
                var newtext = e.Text;

                if (newtext == " ")
                {
                    e.Handled = true;
                }
                else
                {
                    var numtest = Int32.Parse(newtext);
                    //  if (numtest > 65535) e.Handled = true;
                    e.Handled = false;
                }
            }
            catch { e.Handled = true; }
        }

        private void TextBox_PreventSpace(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space) e.Handled = true;
        }

        private void MenuItem_AddWatchlistClick(object sender, RoutedEventArgs e)
        {
            if (NyaaTabControl.SelectedItem == null) return;
            var ntab = NyaaTabControl.SelectedItem as NyaaTab;
            if (ntab == null) return;

 

            Dictionary<int, Tuple<string, string, string>> uniqueNyaa = new Dictionary<int, Tuple<string, string, string>>();
            var allnyaaitems = TraverseNyaaTree(ntab.TreeBase);

            if (allnyaaitems.Count > 0)
            {
                foreach (var oneNyaa in allnyaaitems)
                {
                    var key = oneNyaa.seriesTitle.GetHashCode() + oneNyaa.releaseGroup.GetHashCode() + oneNyaa.qualityToken.GetHashCode();
                    if (uniqueNyaa.ContainsKey(key) == false)
                    {
                        uniqueNyaa.Add(key, new Tuple<string, string, string>(oneNyaa.seriesTitle, oneNyaa.releaseGroup, oneNyaa.qualityToken));
                    }
                }


                var request = "Add the following series to the watchlist? \n";
                foreach (var t in uniqueNyaa.Values)
                {
                    request += "\n" + Tools.ShortenString(t.Item1, "...", 40) + " (" + t.Item2 + ", " + t.Item3 + ")";
                }

                MessageBoxResult result = MessageBox.Show(request, "Add to watchlist", MessageBoxButton.OKCancel);
                if (result == MessageBoxResult.OK)
                {
                    foreach (var t in uniqueNyaa.Values)
                    {
                        Watchlist.AddWatchlistEntry(t.Item1, t.Item2, t.Item3);
                    }
                }
            }
        }



        private void Button_Click_Watchlist_Delete(object sender, RoutedEventArgs e)
        {
            try
            {
                if (((Button)sender).Content.ToString() == "Delete")
                {
                    ((Button)sender).Content = "Confirm delete";
                }
                else
                {
                    var wEntry = ((Button)sender).DataContext as WatchlistEntry;
                    Watchlist.Entries.Remove(wEntry);
                }
                //  
            }
            catch { }

        }



    }












}
