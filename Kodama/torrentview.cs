﻿using System;
using System.ComponentModel;
using MonoTorrent.Client;
using System.Collections.ObjectModel;
using System.IO;

namespace Kodama
{
    class TorrentView
    {
        public static ObservableCollection<TorrentListEntry> TorrentList = new ObservableCollection<TorrentListEntry>();


        public static void ScanTorrentDirectory()
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += worker_DoWork;
            worker.RunWorkerAsync();
        }

        static void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            foreach (string filename in Directory.GetFiles(Settings.All.TorrentPath))
            {
                if (filename.EndsWith(".torrent"))
                {
                    AddTorrentFile(filename);
                }
            }
        }

        

        public static void AddTorrentFile(string filename)
        {
            UIHelper.uiDispatcher.BeginInvoke(new Action(() =>
            {
                TorrentView.TorrentList.Add(new TorrentListEntry(filename));
            }));
        }

        public static void Shutdown()
        {
            TorrentFunctions.fastResume = new MonoTorrent.BEncoding.BEncodedDictionary();

            foreach (var tor in TorrentList)
            {
                tor.TManager.Stop();
                TorrentFunctions.HashTorrent(tor.TManager);
            }
        }



        private static void TorrentFileDownloadCompleted(object sender, AsyncCompletedEventArgs e)
        {
            var filename = (string)e.UserState;
            try
            {
                File.Move(filename, filename + ".torrent");

                TorrentView.AddTorrentFile(filename + ".torrent");

            }
            catch (Exception ex) { Debug.WriteLine("Couldn't download torrent: " + filename + " \n" + ex.ToString()); }
        }


        public static void DownloadTorrentInBackground(SearchNyaa.NyaaItem downloadNyaa)
        {
            Watchlist.AddTorrentToHistory(downloadNyaa.url);
            Tools.DownloadFileInBackground(downloadNyaa.url, Settings.All.TorrentPath + "\\" + SearchNyaa.NyaaTools.RemoveSpecificCharacters(downloadNyaa.title, "?:_\\/\"|"),
                new System.ComponentModel.AsyncCompletedEventHandler(TorrentFileDownloadCompleted));
        }


    }





    class TorrentListEntry : INotifyPropertyChanged
    {
        enum FinalState { notfinished, finish, delete };

        public void NotifiyPropertyChanged(string p) { if (PropertyChanged != null)  PropertyChanged(this, new PropertyChangedEventArgs(p)); }
        public event PropertyChangedEventHandler PropertyChanged;

        private string _Label, _Progress, _Group, _Peers, _State, _Speed, _Downloaded;
        public string Label { get { return _Label; } set { _Label = value; NotifiyPropertyChanged("Label"); } }
        public string Progress { get { return _Progress; } set { _Progress = value; NotifiyPropertyChanged("Progress"); } }
        public string Group { get { return _Group; } set { _Group = value; NotifiyPropertyChanged("Group"); } }
        public string Peers { get { return _Peers; } set { _Peers = value; NotifiyPropertyChanged("Peers"); } }
        public string State { get { return _State; } set { _State = value; NotifiyPropertyChanged("State"); } }
        public string Speed { get { return _Speed; } set { _Speed = value; NotifiyPropertyChanged("Speed"); } }
        public string Downloaded { get { return _Downloaded; } set { _Downloaded = value; NotifiyPropertyChanged("Downloaded"); } }
        private double _Percentage; public double Percentage { get { return _Percentage; } set { _Percentage = value; NotifiyPropertyChanged("Percentage"); } }

        private System.Windows.Media.SolidColorBrush _Fgcolor;
        public System.Windows.Media.SolidColorBrush Fgcolor { get { return _Fgcolor; } set { _Fgcolor = value; NotifiyPropertyChanged("Fgcolor"); } }

        public TorrentManager TManager;
        public string torrentfilename;
        FinalState finalState;

        public TorrentListEntry(string torrentfilename)
        {

            this.Label = torrentfilename;
            this.torrentfilename = torrentfilename;
            this.finalState = FinalState.notfinished;

            this.Fgcolor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 150, 150, 150));

            string fileTitle = Path.GetFileNameWithoutExtension(torrentfilename);

            Debug.WriteLine("Trying to load torrent: " + torrentfilename);

            try
            {
                //  this.Group = fileTitle.Substring(0, fileTitle.IndexOf("§")).Trim();


                this.Group = SearchNyaa.NyaaTools.GetSeriesTitle(fileTitle, SearchNyaa.NyaaTools.GetBrackets(fileTitle), true);
            }
            catch { this.Group = "other"; }

            try
            {

                TorrentFunctions.SummonTorrent(torrentfilename, out this.TManager);
                if (TManager.Complete == false)
                {
                    TorrentFunctions.StartTorrent(TManager);
                }
                else
                {
                    finalState = FinalState.finish;
                    try { if (TorrentFunctions.engine.Torrents.Contains(TManager) == false) TorrentFunctions.engine.Register(TManager); }
                    catch (Exception ex) { Debug.WriteLine("Couldn't register already finished torrent: " + ex); }
                    CheckFinalState();
                    return;
                }
                this.Label = TManager.Torrent.Name ?? torrentfilename;

                TManager.PieceHashed += TManager_PieceHashed;
                TManager.PieceManager.BlockReceived += TManager_PieceHashed;

                //TManager.OnPeerFound
                TManager.TorrentStateChanged += TManager_TorrentStateChanged;

                TManager.Engine.StatsUpdate += TManager_PieceHashed;//Engine_StatsUpdate;
                TManager.PeerConnected += TManager_PeerConnected;
                TManager.TorrentStateChanged += TManager_PieceHashed;//TManager_TorrentStateChanged;


                //    this.State = TManager.State.ToString();
                this.Percentage = TManager.Progress;


                int fullsize = (int)(TManager.Torrent.Size / (1024 * 1024));
                this.Downloaded = fullsize.ToString();

                CheckFinalState();
                ChangeColor();
            }
            catch { this.Speed = "Error"; }
        }


        void ChangeColor()
        {
            UIHelper.uiDispatcher.BeginInvoke(new Action(() =>
            {
                if (TManager.State == MonoTorrent.Common.TorrentState.Downloading) this.Fgcolor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 0, 0, 0));
                else this.Fgcolor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 150, 150, 150));
            }));
        }


        void TManager_TorrentStateChanged(object sender, TorrentStateChangedEventArgs e)
        {
            ChangeColor();

            CheckFinalState();
        }


        void CheckFinalState()
        {
            Debug.WriteLine("Torrent state changed, checking new state: " + TManager.State + " " + finalState);
            if (TManager.State == MonoTorrent.Common.TorrentState.Seeding)
            {
                TManager.Stop();
                finalState = FinalState.finish;
            }

            if (TManager.State == MonoTorrent.Common.TorrentState.Stopped && TManager.Complete == true && finalState == FinalState.finish)
            {
                FinallyMoveFinished();
            }
            else if (finalState == FinalState.delete && TManager.State == MonoTorrent.Common.TorrentState.Stopped)
            {
                FinallyDelete();
            }
        }

        void FinallyMoveFinished()
        {
            UIHelper.uiDispatcher.BeginInvoke(new Action(() =>
            {
                TorrentView.TorrentList.Remove(this);
            }));

            try
            {
                TorrentFunctions.UnregisterAndDispose(TManager);
            }
            catch { }
            try
            {
                string origin = Path.Combine(Settings.All.DownloadPath, TManager.Torrent.Name);
                string targetDir = Group;
                if (targetDir.Length < 1) targetDir = "other";
                targetDir = Path.Combine(Settings.All.CompletedPath, targetDir);

                if (Directory.Exists(targetDir) == false) { Directory.CreateDirectory(targetDir); }
                //   TManager.MoveFiles(targetDir, false);
                FileTools.SafelyMovePath(origin, targetDir);

                //   File.Move(TManager.Torrent.TorrentPath, TManager.Torrent.TorrentPath + "complete");
                File.Delete(TManager.Torrent.TorrentPath);
            }
            catch (Exception ex) { Debug.WriteLine("Couldn't finish torrent: " + ex); }
        }




        void FinallyDelete()
        {
            TorrentFunctions.UnregisterAndDispose(TManager);
            FileTools.SafelyDeletePath(Path.Combine(Settings.All.DownloadPath, TManager.Torrent.Name));
        }

        /*  void Engine_StatsUpdate(object sender, StatsUpdateEventArgs e)
          {
              int tspeed = TManager.Monitor.DownloadSpeed / 1000;
              if (tspeed != 0) this.Speed = tspeed.ToString();
              else this.Speed = "";
          }
          */
        /*  void TManager_TorrentStateChanged(object sender, TorrentStateChangedEventArgs e)
          {
              var state = TManager.State.ToString();
              if (state == "Downloading") state = "";
              this.State = state;
          }*/

        void TManager_PeerConnected(object sender, PeerConnectionEventArgs e)
        {
            var tpeers = (TManager.Peers.Seeds + TManager.Peers.Leechs);
            if (tpeers != 0) this.Peers = tpeers.ToString();
            else this.Peers = "";
        }

        void TManager_PieceHashed(object sender, EventArgs e)
        {
            var state = TManager.State.ToString();
            if (TManager.State == MonoTorrent.Common.TorrentState.Downloading)
            {
                int tspeed = TManager.Monitor.DownloadSpeed / 1024;
                if (tspeed != 0) this.Speed = tspeed.ToString();
                else this.Speed = "";
            }
            else
            {
                if (TManager.State == MonoTorrent.Common.TorrentState.Stopped) this.Speed = "";
                else this.Speed = state;
            }

            //     int fullsize = (int)(TManager.Torrent.Size / (1024 * 1024));
            //     int partsize = (int)(TManager.Progress * 0.01 * fullsize);
            //     this.Downloaded = partsize + " / " + fullsize; 


            //  this.Progress = TManager.Progress.ToString()+"%";
            this.Percentage = TManager.Progress;
        }


        public void MenuPause()
        {
            if (TManager.State != MonoTorrent.Common.TorrentState.Paused) { TManager.Pause(); } else { TManager.Start(); }
        }

        public void MenuStart()
        {
            TManager.Start();
        }

        public void MenuStop()
        {
            TManager.Stop();
        }

        public void MenuDelete()
        {
            UIHelper.uiDispatcher.BeginInvoke(new Action(() =>
            {
                TorrentView.TorrentList.Remove(this);
            }));


            try
            {
                // File.Move(TManager.Torrent.TorrentPath, TManager.Torrent.TorrentPath + "complete"); 
                File.Delete(TManager.Torrent.TorrentPath);
            }
            catch { }

            if (TManager.State == MonoTorrent.Common.TorrentState.Error || TManager.State == MonoTorrent.Common.TorrentState.Stopped)
            {
                FinallyDelete();
            }
            else
            {
                TManager.Stop();
                finalState = FinalState.delete;
            }
        }
    }
}
