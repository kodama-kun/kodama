﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
//using System.Diagnostics;
using System.IO;
using System.Text;
using System.Windows;
using System.Xml;

namespace Kodama
{
    public class SearchNyaa
    {
        //	string basequery = "http://www.nyaa.se/?page=rss&term={term}&offset={offset}";

        /*<item>
         <title>[Leopard-Raws] Mikagura Gakuen Kumikyoku - 05 RAW &#40;TVA 1280x720 x264 AAC&#41;.mp4</title>
         <category>Raw Anime</category>
         <link>http://www.nyaa.se/?page=download&#38;tid=687168</link>
         <guid>http://www.nyaa.se/?page=view&#38;tid=687168</guid>
         <description><![CDATA[18 seeder(s), 1 leecher(s), 15388 download(s) - 306.2 MiB - Trusted]]></description>
         <pubDate>Tue, 05 May 2015 18:39:12 +0000</pubDate>
         </item>*/








        public static List<NyaaItem> ParseSinglePage(string term, int offset)
        {
            var result = new List<NyaaItem>();

            string basequery = Settings.All.NyaaBasequery; //settings

            string rawquery = basequery.Replace("{term}", NyaaTools.MakeSaveSearchQuery(term));
            rawquery = rawquery.Replace("{offset}", offset.ToString());

            string rawdata = Tools.GetRawHTML(rawquery);


            try
            {
                XmlDocument xml = new XmlDocument();
                xml.LoadXml(rawdata);
                XmlNodeList xnList = xml.SelectNodes("/rss/channel/item");


                foreach (XmlNode xn in xnList)
                {
                    try
                    {
                        result.Add(new NyaaItem(xn));
                    }
                    catch (Exception ex) { Debug.WriteLine("Invalid search xml node: " + ex); }
                }
            }
            catch { Debug.WriteLine("Invalid XML received"); }

            return result;
        }




        public class NyaaItem
        {
            public string title;
            public string description;
            public string url;
            public string pubdate;
            public string releaseGroup;
            public string qualityToken;
            public string cleanTitle;
            public string seriesTitle;
            public int seeders;
            public int leechers;

            public NyaaItem(XmlNode xn)
            {
                title = xn["title"].InnerText;
                description = xn["description"].InnerText;
                url = xn["link"].InnerText;
                pubdate = xn["pubDate"].InnerText;

                var brackets = NyaaTools.GetBrackets(title);
                releaseGroup = NyaaTools.GetReleaseGroup(title, brackets);
                qualityToken = NyaaTools.getQualityToken(brackets);
                cleanTitle = NyaaTools.GetSeriesTitle(title, brackets, false);
                seriesTitle = NyaaTools.GetSeriesTitle(title, brackets, true);
                NyaaTools.ParseSeeders(description, out seeders, out leechers);
            }

            public NyaaItem(string title, string cleanTitle, string seriesTitle, string url)
            {
                this.title = title;
                this.cleanTitle = cleanTitle;
                this.url = url;
                this.seriesTitle = seriesTitle;
            }
        }



        public class NyaaTools
        {
            public static string MakeSaveSearchQuery(string inp)
            {
                string result = inp;

                result = result.Replace(" ", "+");
                result = System.Uri.EscapeDataString(result);
                return result;
            }

            public static void ParseSeeders(string source, out int seeders, out int leechers)
            {
                string[] parts = source.Split(',');
                seeders = Convert.ToInt32(GetNumbers(parts[0]));
                leechers = Convert.ToInt32(GetNumbers(parts[1]));
            }

            public static string GetNumbers(string source)
            {
                string legalchars = "0123456789.";
                string result = "";
                foreach (char c in source)
                {
                    if (legalchars.Contains(c.ToString())) result += c;
                }
                return result;
            }

            public static List<string> GetBrackets(string inp)
            {
                List<string> result = new List<string>();

                string brackpart = "";
                bool inbracks = false;

                foreach (char c in inp)
                {
                    if (c == '[' || c == '(') { inbracks = true; }
                    else if (c == ']' || c == ')')
                    {
                        inbracks = false;
                        result.Add(brackpart + c);
                        brackpart = "";
                    }

                    if (inbracks == true) { brackpart += c; }
                }
                return result;
            }

            public static string GetReleaseGroup(string title, List<string> bracks)
            {
                foreach (string str in bracks)
                {
                    if (title.IndexOf(str) < 5) { return str; }
                }
                return "";
            }



            public static string RemoveSpecificCharacters(string source, string illegalCharacters)
            {
                //    string illegalCharacters = "1234567890?:_";
                string temp = "";
                for (int i = 0; i < source.Length; i++)
                {
                    char toAdd = source[i];
                    for (int j = 0; j < illegalCharacters.Length; j++)
                    {
                        if (illegalCharacters[j] == source[i])
                        {
                            toAdd = ' ';
                            break;
                        }
                    }
                    if ((int)toAdd > 200) toAdd = ' ';

                    temp += toAdd;
                }
                return temp;
            }


            public static string RemoveDoubleWhitespace(string source)
            {

                string temp = "";
                for (int i = 0; i < source.Length; i++)
                {
                    char toAdd = source[i];

                    if (toAdd != ' ' || (temp.Length > 0 && temp[temp.Length - 1] != ' ')) temp += toAdd;
                }
                return temp;
            }


            public static string GetSeriesTitle(string inp, List<string> bracks, bool removeNumbers)
            {
                string[] trash = { "v0", "v2", "v3", "v4", "v5", "OVA", "ED", "OP", "RAW", "END", " & ", " + ", ".5" };
                string[] case_insensitive_trash = { "Volume", " vol.", " Vol", "NCED", "xdelta3", "Special", "patch", "Complete", "unofficial", "batch", "PV", /*" s ",*/ " s1", " s2", " s3" };
                string[] fileendings = { ".mkv", ".mp4", ".avi", ".m4a", ".torrent", ".rmvb", ".wmv", ".ssa", ".ass", ".mka", ".exe", ".xdelta3", ".zip" };


                string result = ReplaceCaseInsensitive(inp, fileendings, " ");// removeFileEnding(inp); ;
                foreach (string str in bracks)
                {
                    result = result.Replace(str, "");
                }





                foreach (string str in trash)
                {
                    result = result.Replace(str, " ");
                }

                result = ReplaceCaseInsensitive(result, case_insensitive_trash, " ");



                if (result.Contains(" ") == false) result = result.Replace(".", " ");


                if (removeNumbers == true) result = RemoveSpecificCharacters(result, "1234567890?:_");
                else result = RemoveSpecificCharacters(result, "?:_");

                result = result.Replace(" - ", " ");

                result = ConvertDiacritics(result);

                result = result.Trim('-');
                result = result.Trim();
                result = result.Trim('+');
                result = result.Trim();
                result = result.Trim('-');
                result = result.Trim();
                result = result.Trim('+');
                result = result.Trim();



                result = RemoveDoubleWhitespace(result);
                result = result.Trim();

                result = new System.Globalization.CultureInfo("en-US", false).TextInfo.ToTitleCase(result);


                return result;
            }

            /*   //this isn't very elegant, I know.
               public static string removeBullshitCharacters(string inp)
               {
                   string result = "";
                   foreach (char c in inp)
                   {
                       if ((int)c < 200) result += c;
                       else result += '-';
                   }

                   return result;
               }
               */

            public static string ConvertDiacritics(string text)
            {
                var normalizedString = text.Normalize(NormalizationForm.FormD);
                var stringBuilder = new StringBuilder();

                foreach (var c in normalizedString)
                {
                    var unicodeCategory = System.Globalization.CharUnicodeInfo.GetUnicodeCategory(c);
                    if (unicodeCategory != System.Globalization.UnicodeCategory.NonSpacingMark)
                    {
                        stringBuilder.Append(c);
                    }
                }
                return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
            }


            /*
            public static string removeFileEnding(string inp)
            {
                string[] trash = { "mkv", "mp4", "avi", "m4a", "torrent", "rmvb", "wmv", "ssa", "ass", "mka", "exe", "xdelta3", "zip" };

                foreach (string str in trash)
                {
                    if (inp.ToLower().Contains(str.ToLower()))
                    {
                        inp = inp.Replace("." + str, " ");
                        inp = inp.Replace("." + str.ToLower(), " ");
                        inp = inp.Replace("." + str.ToUpper(), " ");
                    }
                }

                return inp;
            }*/

            public static string ReplaceCaseInsensitive(string source, string[] toReplace, string replaceWith)
            {
                string result = source;
                string lowersource = source.ToLower();

                for (int i = 0; i < toReplace.Length; i++)
                {
                    string lowerReplace = toReplace[i].ToLower();
                    int replaceStart = lowersource.IndexOf(lowerReplace);
                    if (replaceStart != -1)
                    {
                        string temp = result.Substring(0, replaceStart);
                        temp += replaceWith;
                        temp += result.Substring(replaceStart + lowerReplace.Length);

                        result = temp;
                        lowersource = result.ToLower();
                    }
                }

                return result;
            }

            static string[] qualitokens;
            public static string getQualityToken(List<string> bracks)
            {
                string qualitokens_encoded = "720p|480p|1080p|360p|1920x|1280x|720x|848x|704x|480x|x480|10bit|8bit|Complete|Vol.|Soundtrack|Bluray|FLAC|Blu-Ray|h264|xvid|aac|Volume|BDRip";
                if (qualitokens == null)
                {
                    qualitokens = qualitokens_encoded.Split('|');
                    for (int i = 0; i < qualitokens.Length; i++) qualitokens[i] = qualitokens[i].ToLower();
                }

                foreach (string str in bracks)
                {
                    var lowerstr = str.ToLower();
                    foreach (string tkn in qualitokens)
                    {
                        if (lowerstr.Contains(tkn)) { return str; }
                    }
                }
                return "";
            }
        }


    }







    public class NyaaTab : INotifyPropertyChanged
    {
        private string _Header, _HeaderStats, _Content, _ResultList;
        public string Header { get { return _Header; } set { _Header = value; NotifyPropertyChanged("Header"); } }
        public string HeaderStats { get { return _HeaderStats; } set { _HeaderStats = value; NotifyPropertyChanged("HeaderStats"); } }
        public string Content { get { return _Content; } set { _Content = value; NotifyPropertyChanged("Content"); } }

        public string ResultList { get { return _ResultList; } set { _ResultList = value; NotifyPropertyChanged("ResultList"); } }

        //    public string MenuTextWatchlist { get; set; }

        private NyaaResultKnownRG _TreeBase;
        public ObservableCollection<NyaaResult> TreeBase
        {
            get { return _TreeBase.children; }
            //   set { _TreeBase.children = value; }
        }

        public NyaaTab ThisTab { get; set; }

        // public static Dispatcher uiDispatcher;
        private int numResults, goodResults;

        public BackgroundWorker worker, bakaWorker;

        private string _workerArguments;
        public string workerArguments { get { return _workerArguments; } set { _workerArguments = value; NotifyPropertyChanged("workerArguments"); } }

        public void NotifyPropertyChanged(string property) { if (PropertyChanged != null)  PropertyChanged(this, new PropertyChangedEventArgs(property)); }
        public event PropertyChangedEventHandler PropertyChanged;

        private string ReleaseGroup, Quality;


        public NyaaTab(string header, string workerArguments, string ReleaseGroup, string Quality, bool startNow)
        {
            this._TreeBase = new NyaaResultKnownRG();
            this.Header = header;
            this.workerArguments = workerArguments;
            ThisTab = this;

            //  this.MenuTextWatchlist = "asdasd";

            this.ReleaseGroup = ReleaseGroup;
            this.Quality = Quality;

            //   uiDispatcher = Dispatcher.CurrentDispatcher;

            if (startNow == true) DoStart();
        }

        public void DoStart()
        {
            this.TreeBase.Clear();
            _TreeBase.AddStandardCategories();
            worker = new BackgroundWorker();
            worker.DoWork += doWork;
            worker.RunWorkerCompleted += workCompleted;
            worker.RunWorkerAsync();

            bakaWorker = new BackgroundWorker();
            bakaWorker.DoWork += doBakaWork;
            bakaWorker.RunWorkerAsync();
        }

        void doWork(object sender, DoWorkEventArgs e)
        {
            var searchQueries = workerArguments.Split('\n');


            HashSet<string> foundTorrents = new HashSet<string>();

            Header = "";

            foreach (string rawSearchQuery in searchQueries)
            {
                if (rawSearchQuery.Length < 3) return;

                var searchQuery = rawSearchQuery;
                if (Quality != "") searchQuery = Quality + " " + searchQuery;
                if (ReleaseGroup != "") searchQuery = ReleaseGroup + " " + searchQuery;


                if (Header.Length > 1) Header += "\n";
                Header += Tools.ShortenString(searchQuery, "...", 20);






                int idx = 1;
                bool continueSearch = true;
                do
                {
                    var results = SearchNyaa.ParseSinglePage(searchQuery, idx);
                    idx++;

                    if (results.Count == 0)
                    {
                        continueSearch = false;
                    }
                    else
                    {
                        foreach (var nyaa in results)
                        {
                            //    var nyaar = new NyaaResult { Label = nyaa.title };
                            //    nyaar.children.Add(new NyaaResult { Label = nyaa.title + " child", Item = nyaa });    
                            if (foundTorrents.Contains(nyaa.url) == false)
                            {
                                numResults++;
                                if (nyaa.seeders > 0) goodResults++;
                                foundTorrents.Add(nyaa.url);
                                AddNyaaResultToChildrenD(new NyaaResult { Label = nyaa.title, Item = nyaa }); //Bgcolor=bgcolor,
                            }
                        }
                    }

                    //tools.ShortenString(workerArguments.Replace("|","\n"), "...", 30);
                    HeaderStats = goodResults + " ok, " + (numResults - goodResults) + " dead";
                } while (continueSearch);



            }
        }


        public static List<SearchNyaa.NyaaItem> BackgroundNyaaSearch(string searchQuery, string releasegroup, string quality)
        {
            List<SearchNyaa.NyaaItem> results = new List<SearchNyaa.NyaaItem>();
            int idx = 1;
            bool continueSearch = true;
            do
            {
                var searchresults = SearchNyaa.ParseSinglePage(searchQuery + " " + releasegroup + " " + quality, idx);
                idx++;

                if (searchresults.Count == 0)
                {
                    continueSearch = false;
                }
                else
                {
                    foreach (var nyaa in searchresults)
                    {
                        if (nyaa.seriesTitle == searchQuery && nyaa.releaseGroup.Contains(releasegroup) && nyaa.qualityToken.Contains(quality)) results.Add(nyaa);
                    }
                }
            } while (continueSearch);
            return results;
        }


        void doBakaWork(object sender, DoWorkEventArgs e)
        {
            var searchQueries = workerArguments.Split('\n');
            HashSet<string> foundTorrents = new HashSet<string>();


            foreach (string searchQuery in searchQueries)
            {
                foreach (var bakaResult in BakaSearch.SearchForTitle(searchQuery, 4))
                {
                    if (foundTorrents.Contains(bakaResult.title) == false) //titles only for now
                    {
                        foundTorrents.Add(bakaResult.title);
                        UIHelper.uiDispatcher.BeginInvoke(new Action(() =>
                        {
                            TreeBase[4].children.Add(new NyaaResult { Label = bakaResult.title, Item = bakaResult });
                        }));
                    }
                }
            }
        }
        /*
        void AddNyaaResultD(NyaaResult source)
        {
            uiDispatcher.BeginInvoke(new Action(() =>
            {
                TreeBase.Add(source);
            }));
        }
        */
        void AddNyaaResultToChildrenD(NyaaResult source)
        {
            UIHelper.uiDispatcher.BeginInvoke(new Action(() =>
            {
                source.Bgcolor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 220, 255, 220));
                if (source.Item.seeders < 5) source.Bgcolor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 255, 255, 220));
                if (source.Item.seeders == 0) source.Bgcolor = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(255, 255, 220, 220));
                _TreeBase.AddToChildren(source);
            }));
        }

        void workCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }
    }




    public class NyaaResultKnownRG : NyaaResult
    {
        HashSet<string> knownRGs;

        public NyaaResultKnownRG()
        {
            knownRGs = new HashSet<string>();

            //  string rawRGs = "Commie|HorribleSubs|Coalgirls|Chihiro|Polished|Zurako|FFF|DeadFish|GotWoot|CMS|WhyNot|GG|final8|riycou|Exiled-Destiny|Vivid|DmonHiro|FedSubs|utw";

            foreach (var rg in Settings.All.ReleaseGroups)
            {
                knownRGs.Add("[" + rg.ToLower() + "]");
            }

            children = new ObservableCollection<NyaaResult>();

        }

        public void AddStandardCategories()
        {
            children.Add(new NyaaResultRG { Label = "Known releasegroup", Expanded = true });
            children.Add(new NyaaResultRG { Label = "Unknown releasegroup" });
            children.Add(new NyaaResultRG { Label = "Known releasegroup (dead)" });
            children.Add(new NyaaResultRG { Label = "Unknown releasegroup (dead)" });
            children.Add(new NyaaResultRG { Label = "Baka" });
        }

        public override void AddToChildren(NyaaResult source)
        {
            if (knownRGs.Contains(source.Item.releaseGroup.ToLower()))
            {
                if (source.Item.seeders > 0)
                {
                    children[0].AddToChildren(source);
                }
                else { children[2].AddToChildren(source); }
            }
            else
            {
                if (source.Item.seeders > 0) { children[1].AddToChildren(source); }
                else { children[3].AddToChildren(source); }
            }
        }
    }









    public class NyaaResultRG : NyaaResult
    {
        private Dictionary<string, int> cIndex;

        public NyaaResultRG()
        {
            cIndex = new Dictionary<string, int>();

            CheckboxVisibility = System.Windows.Visibility.Collapsed;
        }

        public override void AddToChildren(NyaaResult source)
        {
            int ci = -1;
            if (cIndex.TryGetValue(source.Item.releaseGroup, out ci) == false)
            {
                ci = children.Count;
                children.Add(new NyaaResultSeries(source));
                cIndex.Add(source.Item.releaseGroup, ci);
            }
            children[ci].AddToChildren(source);

        }
    }




    public class NyaaResultSeries : NyaaResult
    {
        private Dictionary<string, int> cIndex;


        public NyaaResultSeries(NyaaResult source)
        {
            cIndex = new Dictionary<string, int>();
            base.Label = source.Item.releaseGroup;
            if (base.Label == " " || base.Label == "") base.Label = "No Releasegroup found";

            base.CheckboxVisibility = System.Windows.Visibility.Collapsed;
            Boldness = FontWeights.Bold;
        }

        public override void AddToChildren(NyaaResult source)
        {
            int ci = -1;
            if (cIndex.TryGetValue(source.Item.seriesTitle, out ci) == false)
            {
                ci = children.Count;
                children.Add(new NyaaResultQuality(source));
                cIndex.Add(source.Item.seriesTitle, ci);
            }

            children[ci].AddToChildren(source);
        }
    }





    public class NyaaResultQuality : NyaaResult
    {
        private Dictionary<string, int> cIndex;

        public NyaaResultQuality(NyaaResult source)
        {
            cIndex = new Dictionary<string, int>();
            base.Label = source.Item.seriesTitle;
            if (base.Label == " " || base.Label == "") base.Label = "No series name found";

            base.CheckboxVisibility = System.Windows.Visibility.Collapsed;
            Boldness = FontWeights.Bold;
        }

        public override void AddToChildren(NyaaResult source)
        {
            int ci = -1;
            if (cIndex.TryGetValue(source.Item.qualityToken, out ci) == false)
            {
                ci = children.Count;
                string qualityLabel = source.Item.qualityToken;
                if (qualityLabel == "" || qualityLabel == " ") qualityLabel = "unknown quality";
                children.Add(new NyaaResult { Label = qualityLabel, Expanded = true });
                cIndex.Add(source.Item.qualityToken, ci);
            }

            children[ci].children.Add(source);

        }
    }




    public class NyaaResult : INotifyPropertyChanged
    {
        public string Label { get; set; }
        public System.Windows.Media.SolidColorBrush Bgcolor { get; set; }
        public SearchNyaa.NyaaItem Item { get; set; }
        public ObservableCollection<NyaaResult> children { get; set; }
        public bool Expanded { get; set; }
        public System.Windows.Visibility CheckboxVisibility { get; set; }
        public FontWeight Boldness { get; set; }

        bool _isChecked = false;

        public bool IsChecked
        {
            get { return _isChecked; }
            set { SetIsChecked(value); }
        }

        void SetIsChecked(bool value)
        {
            // if (value == _isChecked) return;
            _isChecked = value;

            for (int i = 0; i < children.Count; i++)
            {
                children[i].IsChecked = value;
            }


            NotifyPropertyChanged("IsChecked");
        }

        public NyaaResult()
        {
            children = new ObservableCollection<NyaaResult>();
            //  Bgcolor = new System.Windows.Media.SolidColorBrush(Colors.White);
            CheckboxVisibility = System.Windows.Visibility.Visible;

        }

        public virtual void AddToChildren(NyaaResult source)
        {
            children.Add(source);
        }

        void NotifyPropertyChanged(string info) { if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs(info)); } }
        public event PropertyChangedEventHandler PropertyChanged;

    }
}
