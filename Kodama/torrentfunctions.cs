﻿using System;
using System.Collections.Generic;

using MonoTorrent.Common;
using MonoTorrent.Client;
using MonoTorrent.BEncoding;
using MonoTorrent.Client.Encryption;
using MonoTorrent.Dht;
using MonoTorrent.Dht.Listeners;
using System.IO;
using System.Net;
//using System.Diagnostics;

namespace Kodama
{



    class TorrentFunctions
    {

        public static ClientEngine engine;
        public static BEncodedDictionary fastResume;


        public static KeyValuePair<BEncodedString, BEncodedValue> getHashPair(TorrentManager TManager)
        { return new KeyValuePair<BEncodedString, BEncodedValue>(TManager.Torrent.InfoHash.ToHex(), TManager.SaveFastResume().Encode()); }


        public static void HashTorrent(TorrentManager TManager)
        {
            try
            {
                TorrentFunctions.fastResume.Add(TorrentFunctions.getHashPair(TManager));
            }
            catch { }
        }


        public static void SummonTorrent(string filename, out TorrentManager TManager)
        {
            try
            {
                Torrent trrnt = Torrent.Load(filename);
                TManager = new TorrentManager(trrnt, Settings.All.DownloadPath, new TorrentSettings(4, 150, 100000000, 100000000));
                if (fastResume.ContainsKey(trrnt.InfoHash.ToHex())) TManager.LoadFastResume(new FastResume((BEncodedDictionary)fastResume[trrnt.infoHash.ToHex()]));

                Debug.WriteLine("Loaded a torrent: " + filename);

            }
            catch { TManager = null; }
        }


        public static void StartTorrent(TorrentManager TManager)
        {
            try { if (engine.Torrents.Contains(TManager) == false) engine.Register(TManager); }
            catch { }
            try { if (TManager.State == TorrentState.Stopped || TManager.State == TorrentState.Paused) TManager.Start(); }
            catch { }
        }





        public static void StartEngine()
        {
            EngineSettings engineSettings = new EngineSettings(Settings.All.DownloadPath, Settings.All.ListenPort);
            engineSettings.PreferEncryption = false;
            engineSettings.AllowedEncryption = EncryptionTypes.All;

            engine = new ClientEngine(engineSettings);
            engine.ChangeListenEndpoint(new IPEndPoint(IPAddress.Any, Settings.All.ListenPort));

            //DHT
            byte[] nodes = null;
            try { nodes = File.ReadAllBytes(Settings.All.DhtNodeFile); } catch { }

            DhtListener dhtListner = new DhtListener(new IPEndPoint(IPAddress.Any, Settings.All.ListenPort));//Settings.All.DhtPort
            DhtEngine dht = new DhtEngine(dhtListner);
            engine.RegisterDht(dht);
            dhtListner.Start();
            engine.DhtEngine.Start(nodes);


            try 
            {
                fastResume = BEncodedValue.Decode<BEncodedDictionary>(File.ReadAllBytes(Settings.All.FastResumeFile));
                File.Delete(Settings.All.FastResumeFile); //if the program crashes, hash everything on next start!
            }
            catch { fastResume = new BEncodedDictionary(); }

            engine.StatsUpdate += engine_StatsUpdate;
        }

        static void engine_StatsUpdate(object sender, StatsUpdateEventArgs e)
        {
            try { SetApproxUpDownSpeedLimits(); }
            catch { }//this might fail when a torrent finishes and gets removed while in the cycle
        }




        public static void StopEngine()
        {
            File.WriteAllBytes(Settings.All.DhtNodeFile, engine.DhtEngine.SaveNodes());
            File.WriteAllBytes(Settings.All.FastResumeFile, fastResume.Encode());
            engine.Dispose();
            System.Threading.Thread.Sleep(1500);
        }


        public static void UnregisterAndDispose(TorrentManager TManager)
        {
            try
            {
                if (engine.Torrents.Contains(TManager) == true) engine.Unregister(TManager);
                fastResume.Remove(TManager.Torrent.InfoHash.ToHex());
                TManager.Dispose();
            }
            catch (Exception ex) { Debug.WriteLine("Couldn't UnregAndDisp torrent. " + ex); }
        }




/*
        private static int GetTotalDownloadSpeed()
        {
            int result = 0;
            foreach (var t in engine.Torrents)
            {
                if (t.State == TorrentState.Downloading)
                {
                    result += t.Monitor.DownloadSpeed;
                }
            }
            return result;
        }

        private static int GetTotalUploadSpeed()
        {
            int result = 0;
            foreach (var t in engine.Torrents)
            {
                if (t.State == TorrentState.Downloading || t.State == TorrentState.Seeding)
                {
                    result += t.Monitor.UploadSpeed;
                }
            }
            return result;
        }
        */
        public static int TotalDownloadSpeed;
        public static int TotalUploadSpeed;
        private static int PerTorrentDownLimit = 0;
        private static int PerTorrentUpLimit = 0;
        private static void SetApproxUpDownSpeedLimits()
        {
           // TotalDownloadSpeed = (GetTotalDownloadSpeed() / 1024);
          //  TotalUploadSpeed = (GetTotalUploadSpeed() / 1024);

            TotalDownloadSpeed = 0;
            TotalUploadSpeed = 0;

            int CurrentlyActiveTorrents = 0;
            foreach (var t in engine.Torrents)
            {
                if (t.State == TorrentState.Downloading)
                {
                    TotalDownloadSpeed += t.Monitor.DownloadSpeed;
                    
                }
                if (t.State == TorrentState.Downloading || t.State == TorrentState.Seeding)
                {
                    TotalUploadSpeed += t.Monitor.UploadSpeed;

                    if (t.Monitor.DownloadSpeed > 1000) 
                    {
                        CurrentlyActiveTorrents++;
                        if (CurrentlyActiveTorrents > Settings.All.MaxActiveTorrents)
                        {
                            t.Stop();
                        }
                    }
                }
                if (t.State == TorrentState.Stopped && t.Complete == false && File.Exists(t.Torrent.TorrentPath))
                {
                    if (CurrentlyActiveTorrents < Settings.All.MaxActiveTorrents)
                    {
                        t.Start();
                    }
                }
            }

            TotalDownloadSpeed = TotalDownloadSpeed / 1024;
            TotalUploadSpeed = TotalUploadSpeed / 1024;

            //autodetect connection speed
            if (Settings.All.ConnectionDownSpeed < TotalDownloadSpeed) Settings.All.ConnectionDownSpeed = TotalDownloadSpeed;
            if (Settings.All.ConnectionUpSpeed < TotalUploadSpeed) Settings.All.ConnectionUpSpeed = TotalUploadSpeed;


            if (Settings.All.DownLimit != 0)
            {
                if (TotalDownloadSpeed > Settings.All.DownLimit)
                {
                    PerTorrentDownLimit -= 2500; 
                }
                else if (TotalDownloadSpeed < Settings.All.DownLimit)
                {
                    if ((double)TotalDownloadSpeed / (double)Settings.All.DownLimit < 0.25) PerTorrentDownLimit *= 2; 
                    PerTorrentDownLimit += 500; 
                }

                if (PerTorrentDownLimit < 10) PerTorrentDownLimit = 10;
                else if (PerTorrentDownLimit > (Settings.All.DownLimit * 1024)) PerTorrentDownLimit = (Settings.All.DownLimit * 1024);
            }
           
            if (Settings.All.UpLimit != 0)
            {
                if (TotalUploadSpeed > Settings.All.UpLimit) PerTorrentUpLimit -= 50;
                else if (TotalUploadSpeed < Settings.All.UpLimit) PerTorrentUpLimit += 20;

                if (PerTorrentUpLimit < 1) PerTorrentUpLimit = 1;
                else if (PerTorrentUpLimit > (Settings.All.UpLimit * 1024)) PerTorrentUpLimit = (Settings.All.UpLimit * 1024);
            
            }
         
            try
            {
                if (PerTorrentDownLimit != 0 || PerTorrentUpLimit != 0)
                {
                    if (Settings.All.DownLimit == 0) PerTorrentDownLimit = 0;
                    if (Settings.All.UpLimit == 0) PerTorrentUpLimit = 0;

                    foreach (TorrentManager TManager in engine.Torrents)
                    {
                        if (TManager.State == TorrentState.Downloading || TManager.State == TorrentState.Seeding)
                        {
                            TManager.Settings.MaxDownloadSpeed = PerTorrentDownLimit;
                            TManager.Settings.MaxUploadSpeed = PerTorrentUpLimit;
                        }
                        else if (TManager.State == TorrentState.Paused)
                        {
                            TManager.Settings.MaxDownloadSpeed = 1;
                            TManager.Settings.MaxUploadSpeed = 1;
                        }
                    }
                }
            }
            catch (Exception ex) { Debug.WriteLine("Couldn't set speed limits. " + ex); }


        }
    }
}
