﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
//using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Timers;
using System.Xml.Serialization;

namespace Kodama
{




    public class Watchlist
    {
        private static WatchlistManager Tracker;

        public static ObservableCollection<WatchlistEntry> Entries { get { return Tracker.Entries; } set { Tracker.Entries = value; } }


        private static Timer timer;

        private static string watchlistPath = "watchlist.txt";

        public static void Load()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(WatchlistManager));

                using (StreamReader reader = new StreamReader(watchlistPath))
                {
                    Tracker = (WatchlistManager)serializer.Deserialize(reader);
                    reader.Close();
                }

            }
            catch(Exception ex)
            {
                Debug.WriteLine("Couldn't load watchlist: " + ex.ToString());
                Tracker = new WatchlistManager(); }

           
                timer = new Timer();
                timer.Interval = (1000 * 60 * 60) * 0.05; // short timespan for startup
                timer.AutoReset = true;
                timer.Elapsed += timer_Elapsed;
                timer.Start();

        }




        static void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Debug.WriteLine("Timer elapsed! starting automatic search. " + DateTime.Now.ToString());

            timer.Interval = (1000 * 60 * 60) * Settings.All.WatchlistSearchFrequencyInHours; 

            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += worker_DoWork;
            worker.RunWorkerAsync();
        }

        static void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            Save(); // won't hurt to occasionally save the watchlist if the program is constantly running

            var wEntries = Tracker.Entries.ToArray(); // don't care about the array but I want a local copy for this thread to work on.

            foreach (var WE in wEntries)
            {
                Debug.WriteLine("Background searching for: " + WE.Series);

                var allResults = NyaaTab.BackgroundNyaaSearch(WE.Series, WE.ReleaseGroup, WE.Quality);

                foreach (var r in allResults)
                {
                    if (IsTorrentHistoric(r.url) == false)
                    {
                        Debug.WriteLine("Not historic, adding download: " + r.title);
                        TorrentView.DownloadTorrentInBackground(r);
                    }
                }
            }
        }



        public static void Save()
        {
            var serializer = new XmlSerializer(Tracker.GetType());

            using (var writer = new StreamWriter(watchlistPath))
            {
                serializer.Serialize(writer, Tracker);
            }
        }

   /*     public static void TestSearch()
        {
            Tools.Log("Testing background search...");
            foreach (var WE in Tracker.Entries)
            {
                Tools.Log("Searching for: " + WE.Series);

                var allResults = NyaaTab.BackgroundNyaaSearch(WE.Series, WE.ReleaseGroup, WE.Quality);

                foreach (var r in allResults)
                {
                    Tools.Log("Background Search result: " + r.title);
                    if (IsTorrentHistoric(r.url) == true) Tools.Log("Torrent is historic!");
                }
            }
        }
        */

        public static void AddWatchlistEntry(string series, string rg, string quality)
        {
            bool entryExists = false;
            foreach(var e in Tracker.Entries)
            {
                if (e.Series == series && e.ReleaseGroup == rg && e.Quality == quality)
                {
                    entryExists = true;
                    break;
                }
            }

            if (entryExists == false)
            {
                Tracker.Entries.Add(new WatchlistEntry(series, rg, quality));
            }
        }


        public static void AddTorrentToHistory(string key)
        {
            var hash = key.GetHashCode();
            try { Tracker.HistoricTorrents.Add(hash); }
            catch { Debug.WriteLine("Torrent already historic"); }
        }

        public static bool IsTorrentHistoric(string key)
        {
            var hash = key.GetHashCode();
            return Tracker.HistoricTorrents.Contains(hash);
        }
    }


    public class WatchlistManager
    {
        public ObservableCollection<WatchlistEntry> Entries;
        public HashSet<int> HistoricTorrents;

        public WatchlistManager()
        {
            this.Entries = new ObservableCollection<WatchlistEntry>();
            this.HistoricTorrents = new HashSet<int>();
        }


    }

    
    public class WatchlistEntry
    {
        public string Series { get; set; }
        public string ReleaseGroup { get; set; }
        public string Quality { get; set; }
        public DateTime Created { get; set; }

        public WatchlistEntry(string Series, string ReleaseGroup, string Quality)
        {
            this.Series = Series;
            this.ReleaseGroup = ReleaseGroup;
            this.Quality = Quality;
            this.Created = DateTime.UtcNow;
        }

        public WatchlistEntry() { }
    }
}
