﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Kodama
{
    public static class Settings
    {
        public static MihoSettings All;

        public static string settingsPath = "settings.txt";

        public static void Load()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(MihoSettings));

                using (StreamReader reader = new StreamReader(settingsPath))
                {
                    All = (MihoSettings)serializer.Deserialize(reader);
                    reader.Close();
                }
            }
            catch { All = new MihoSettings(); }

            CreateDirectories();
        }

        public static void Save()
        {
            var serializer = new XmlSerializer(typeof(MihoSettings));

            using (var writer = new StreamWriter(settingsPath))
            {
                serializer.Serialize(writer, All);
            }
        }

        public static void CreateDirectories()
        {
            if (Directory.Exists(All.TorrentPath) == false) { Directory.CreateDirectory(All.TorrentPath); }
            if (Directory.Exists(All.DownloadPath) == false) { Directory.CreateDirectory(All.DownloadPath); }
            if (Directory.Exists(All.CompletedPath) == false) { Directory.CreateDirectory(All.CompletedPath); }
        }
    }

    public class MihoSettings
    {
        public string TorrentPath = "Torrents";
        public string DownloadPath = "Incomplete";
        public string CompletedPath = "Completed";
        public string DhtNodeFile = "dhtnodes";
        public string FastResumeFile = "fastresume";
        public string NyaaBasequery = "http://www.nyaa.se/?page=rss&term={term}&offset={offset}";
        public string HttpUserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:29.0) Gecko/20120101 Firefox/29.0";
        public int ListenPort = 12345;
   //     public int DhtPort = 12346; //is this even used? I have no idea.
        public int ConnectionDownSpeed = 100;
        public int ConnectionUpSpeed = 10;
        public int DownLimit = 0;
        public int UpLimit = 0;
        public int MaxActiveTorrents = 10;
        public int MaxActiveLimit = 49;
        public double WatchlistSearchFrequencyInHours = 3.0;
        public DateTime LastChartsUpdate = new DateTime(2015, 1, 1);
        public DateTime LastBakaUpdate = new DateTime(2015, 1, 1);

        public bool ShowMoviesAndSpecials = true;
        public bool LogToFile = false;
        public string[] ReleaseGroups = { "Commie", "HorribleSubs", "Coalgirls", "Chihiro", "Polished", "Zurako", "FFF", "DeadFish", "GotWoot", "CMS", "WhyNot",
                                          "GG","final8","riycou","Exiled-Destiny","Vivid","DmonHiro","FedSubs","utw", "Migoto", "Scriptum", "Tsundere", "Nutbladder", "BakedFish" };
    }
}
