﻿using System;
using System.Collections.Generic;
//using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kodama
{
    class Debug
    {
        public static void WriteLine(object source)
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Log: " + source.ToString());

                if (Settings.All.LogToFile == false) return;

                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    w.Write("\r\n" + DateTime.Now + " " + source.ToString());
                }
            }
            catch { }
        }
    }

    class Tools
    {
        /*    public static void Log(object source)
            {
                try
                {
                    Debug.WriteLine("Log: " + source.ToString());

                    if (Settings.All.LogToFile == false) return;

                    using (StreamWriter w = File.AppendText("log.txt"))
                    {
                        w.Write("\r\n" + DateTime.Now + " " + source.ToString());
                    }
                }
                catch { }
            }*/

        public static List<string> MultiRegex(string source, string prefix, string postfix)
        {
            return MultiRegex(source, prefix + "([\\d|\\D]*?)" + postfix);
        }

        public static List<string> MultiRegex(string source, string rx)
        {
            List<string> results = new List<string>();
            try
            {
                System.Text.RegularExpressions.Regex ItemRegex = new System.Text.RegularExpressions.Regex(rx);

                foreach (System.Text.RegularExpressions.Match ItemMatch in ItemRegex.Matches(source))
                {
                    results.Add(ItemMatch.Groups[1].Value);
                }
            }
            catch { }
            return results;
        }

        public static string SingleRegex(string source, string prefix, string postfix)
        {
            return SingleRegex(source, prefix + "([\\d|\\D]*?)" + postfix);
        }

        public static string SingleRegex(string source, string rx)
        {
            string result = "";



            System.Text.RegularExpressions.Regex ItemRegex = new System.Text.RegularExpressions.Regex(rx);
            System.Text.RegularExpressions.Match match = ItemRegex.Match(source);
            if (match.Success)
            {
                result = match.Groups[1].Value;
            }
            return result;
        }


        public static string GetRawHTML(string url)
        {
            string result = "";

            //    log("Downloading url as string: " + url);
            using (var webClient = new System.Net.WebClient())
            {
                webClient.Encoding = Encoding.UTF8;
                webClient.Headers.Add("user-agent", Settings.All.HttpUserAgent);
                try { result = webClient.DownloadString(url); }
                catch { }
            }

            return result;
        }


        public static void DownloadFileInBackground(string url, string filename, System.ComponentModel.AsyncCompletedEventHandler Completed)
        {
            using (var client = new System.Net.WebClient())
            {
                client.Headers.Add("user-agent", Settings.All.HttpUserAgent);
                if (Completed != null) client.DownloadFileCompleted += Completed;
                client.DownloadFileAsync(new Uri(url), filename, filename);
            }
        }

        public static void DownloadFile(string url, string filename)
        {
            using (var client = new System.Net.WebClient())
            {
                client.Headers.Add("user-agent", Settings.All.HttpUserAgent);
                client.DownloadFile(new Uri(url), filename);
            }
        }


        public static string ShortenString(string source, string dotdotdot, int maxlen)
        {
            if (source.Length < maxlen) return source;
            return source.Substring(0, maxlen) + dotdotdot;
        }



        static System.Drawing.Imaging.ImageCodecInfo encoder;
        public static async Task DownloadAndResizeImage(string URL, string NewFile, int NewWidth, int MaxHeight, bool OnlyResizeIfWider)
        {
            if (System.IO.Directory.Exists("dbimages") == false) System.IO.Directory.CreateDirectory("dbimages");

            using (var client = new System.Net.WebClient())
            {
                using (var stream = await client.OpenReadTaskAsync(URL))
                {
                    using (System.Drawing.Image FullsizeImage = System.Drawing.Image.FromStream(stream))
                    {
                        // Prevent using images internal thumbnail
                        FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);
                        FullsizeImage.RotateFlip(System.Drawing.RotateFlipType.Rotate180FlipNone);

                        if (OnlyResizeIfWider)
                        {
                            if (FullsizeImage.Width <= NewWidth)
                            {
                                NewWidth = FullsizeImage.Width;
                            }
                        }
                        int NewHeight = FullsizeImage.Height * NewWidth / FullsizeImage.Width;
                        if (NewHeight > MaxHeight)
                        {
                            NewWidth = FullsizeImage.Width * MaxHeight / FullsizeImage.Height;
                            NewHeight = MaxHeight;
                        }

                        System.Drawing.Image NewImage = FullsizeImage.GetThumbnailImage(NewWidth, NewHeight, null, IntPtr.Zero);
                        FullsizeImage.Dispose();

                        if (encoder == null) encoder = System.Drawing.Imaging.ImageCodecInfo.GetImageEncoders().FirstOrDefault(t => t.MimeType == "image/jpeg");
                        var eps = new System.Drawing.Imaging.EncoderParameters(1);
                        eps.Param[0] = new System.Drawing.Imaging.EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 90L);
                        NewImage.Save(NewFile, encoder, eps);
                    }
                }
            }
        }


        public static async Task DownloadAndCheckImage(string URL, string NewFile)
        {
            if (System.IO.Directory.Exists("dbimages") == false) System.IO.Directory.CreateDirectory("dbimages");

            using (var client = new System.Net.WebClient())
            {
                byte[] response = await client.DownloadDataTaskAsync(URL);
                using (Stream stream = new MemoryStream(response))
                {
                    using (System.Drawing.Image FullsizeImage = System.Drawing.Image.FromStream(stream))
                    {
                        // Debug.WriteLine(FullsizeImage.Width + "x" + FullsizeImage.Height);
                    }
                }
                File.WriteAllBytes(NewFile, response);
            }
        }



        public static string GetVersionString()
        {
            //remember to set your assemblyinfo.cs to [assembly: AssemblyVersion("1.0.*")]
            var version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            var buildDateTime = new DateTime(2000, 1, 1).Add(new TimeSpan(TimeSpan.TicksPerDay * version.Build + // days since 1 January 2000
                                                                          TimeSpan.TicksPerSecond * 2 * version.Revision));

            string asmblname = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            string vstring = "v" + (buildDateTime.Year - 2014) + "." + buildDateTime.ToString("MMdd.HH");
            //   string vstring = "v" + version.ToString();

            //return asmblname + "\nwtfpl, " + vstring;
            return asmblname + " " + vstring + "\nHave an excellent day!";
        }



        public static int LevenshteinDistance(string s, string t)
        {
            s = s.ToLower();
            t = t.ToLower();

            int n = s.Length;
            int m = t.Length;
            int[,] d = new int[n + 1, m + 1];

            if (n == 0) return m;
            if (m == 0) return n;

            for (int i = 0; i <= n; d[i, 0] = i++) { }
            for (int j = 0; j <= m; d[0, j] = j++) { }

            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= m; j++)
                {
                    int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;
                    d[i, j] = Math.Min(
                        Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
                        d[i - 1, j - 1] + cost);
                }
            }
            return d[n, m];
        }

    }




    public static class FileTools
    {

        static void SafelyMoveDirectory(string origin, string target)
        {
            try
            {
                DirectoryMoveMerge(origin, target);
            }
            catch (Exception ex) { Debug.WriteLine(ex.ToString()); }
        }



        static void SafelyMoveFile(string origin, string target) //target must be a path with filename
        {
            try
            {
                //file already exists? Overwrite it.
                if (File.Exists(target) == true) File.Delete(target);

                //generate target directory
                string targetDirectory = Path.GetDirectoryName(target);
                if (Directory.Exists(targetDirectory) == false)
                {
                    Directory.CreateDirectory(targetDirectory);
                }

                //Move or copy file
                if (ArePathsOnSameDrive(origin, target))
                {
                    File.Move(origin, target);
                }
                else
                {
                    File.Copy(origin, target);
                    File.Delete(origin);
                }
            }
            catch (Exception ex) { Debug.WriteLine(ex.ToString()); }
        }





        public static void SafelyMovePath(string origin, string target)
        {

            if (IsPathAFile(origin))
            {
                //SafelyMoveFile(origin, target);
                var filename = Path.GetFileName(origin);
                var filetarget = target;
                if (filetarget.EndsWith(filename) == false) filetarget = Path.Combine(filetarget, filename);
                SafelyMoveFile(origin, filetarget);
            }
            else
            {
                SafelyMoveDirectory(origin, target);
            }
        }




        public static string GetDriveRoot(string inputPath)
        {
            try
            {
                FileInfo file = new FileInfo(inputPath);
                return file.Directory.Root.FullName;

            }
            catch (Exception ex) { Debug.WriteLine(ex.ToString()); return ""; }
        }




        public static bool ArePathsOnSameDrive(string path1, string path2)
        {
            if (GetDriveRoot(path1) == GetDriveRoot(path2)) return true;
            return false;
        }




        public static bool IsPathAFile(string inputPath)
        {
            try
            {
                FileAttributes attr = File.GetAttributes(inputPath);
                if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                {
                    return false;
                }
            }
            catch { }
            return true;
        }




        /*
        public static int getLongestSubPath(string inputPath)
        {
            int pathlength = inputPath.Length;
            if (isPathAFile(inputPath) == true) return pathlength;

            DirectoryInfo source = new DirectoryInfo(inputPath);

            foreach (FileInfo fi in source.GetFiles())
            {
                if (fi.FullName.Length > pathlength) pathlength = fi.FullName.Length;
            }

            // Continue for all directories
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                int sublength = getLongestSubPath(diSourceSubDir.FullName);
                if (sublength > pathlength) pathlength = sublength;
            }
            return pathlength;

        }
        */

        /*
        public static void safelyMovePathImploding(string origin, string target, string basepath)
        {


            if (isPathAFile(origin))
            {
                safelyMoveFileImploding(origin, target);
            }
            else
            {
                safelyMoveDirectoryImploding(origin, basepath);
            }
        }
        */
        /*
        static void safelyMoveFileImploding(string origin, string target) //target must be a path with filename
        {
            try
            {
                FileInfo file = new FileInfo(target);
                string fname = file.Name;

                string newfname = magicallyShortenFilename(fname, settings.maxTorrentNameLength);

                target = target.Replace(fname, newfname);

                safelyMoveFile(origin, target);

            }
            catch (Exception ex) { Debug.WriteLine(ex.ToString()); }
        }
        */


        /*
                static void safelyMoveDirectoryImploding(string origin, string target)
                {
                    try
                    {
                        DirectoryMoveMergeImploding(origin, target);
                    }
                    catch (Exception ex) { Debug.WriteLine(ex.ToString()); }
                }
                */

        /*
        private static void DirectoryMoveMergeImploding(string sourcePath, string targetPath)
        {
            if (sourcePath == targetPath) return;

            DirectoryInfo source = new DirectoryInfo(sourcePath);
            DirectoryInfo target = new DirectoryInfo(targetPath);

            // Check if the target directory exists, if not, create it.
            if (Directory.Exists(target.FullName) == false)
            {
                Directory.CreateDirectory(target.FullName);
            }

            // Copy each file into it's new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                safelyMoveFileImploding(fi.FullName, Path.Combine(target.ToString(), fi.Name));
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryMoveMergeImploding(diSourceSubDir.FullName, targetPath);
            }

            try
            {
                //this directory should now be empty, remove it
                Directory.Delete(source.FullName);
            }
            catch (Exception ex) { Debug.WriteLine(ex.ToString()); }
        }
        */









        private static void DirectoryMoveMerge(string sourcePath, string targetPath)
        {
            if (sourcePath == targetPath) return;

            DirectoryInfo source = new DirectoryInfo(sourcePath);
            DirectoryInfo target = new DirectoryInfo(targetPath);

            // Check if the target directory exists, if not, create it.
            if (Directory.Exists(target.FullName) == false)
            {
                Directory.CreateDirectory(target.FullName);
            }

            // Copy each file into it's new directory.
            foreach (FileInfo fi in source.GetFiles())
            {
                SafelyMoveFile(fi.FullName, Path.Combine(target.ToString(), fi.Name));
                //fi.CopyTo(Path.Combine(target.ToString(), fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
                DirectoryMoveMerge(diSourceSubDir.FullName, nextTargetSubDir.FullName);
            }

            try
            {
                //this directory should now be empty, remove it
                Directory.Delete(source.FullName);
            }
            catch (Exception ex) { Debug.WriteLine(ex.ToString()); }
        }




        public static void SafelyDeletePath(string origin)
        {
            if (IsPathAFile(origin))
            {
                try { File.Delete(origin); }
                catch (Exception ex) { Debug.WriteLine(ex.ToString()); }
            }
            else
            {
                DirectoryDelete(origin);
            }
        }



        private static void DirectoryDelete(string sourcePath)
        {
            DirectoryInfo source = new DirectoryInfo(sourcePath);

            // What is this I don't even
            if (Directory.Exists(source.FullName) == false) return;

            foreach (FileInfo fi in source.GetFiles())
            {
                try { fi.Delete(); }
                catch (Exception ex) { Debug.WriteLine(ex.ToString()); }
            }

            // Continue for all directories
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryDelete(diSourceSubDir.FullName);
            }

            try { Directory.Delete(source.FullName); }
            catch (Exception ex) { Debug.WriteLine(ex.ToString()); }
        }





        /*
        //targetlength means the new length of the substring, not the whole filename.
        public static string magicallyShortenFilename(string source, int targetlength)
        {
            if (source.Length < targetlength || targetlength < 10) return source;

            string longestmatch = "";


            //first, try to remove checksums

            Regex checksumRegex = new Regex("(\\s\\[[\\w]{8}\\])");
            Match match = checksumRegex.Match(source);
            if (match.Success)
            {
                string toremove = match.Groups[1].Value;
                source = source.Replace(toremove, "");
            }


            //next, try to find the longest consecutive non-numeric part of the filename, then shorten it
            try
            {
                Regex ItemRegex = new Regex("([\\D|\\s]+).");

                foreach (Match ItemMatch in ItemRegex.Matches(source))
                {
                    string candidate = ItemMatch.Groups[1].Value;
                    if (candidate.Length > longestmatch.Length) longestmatch = candidate;
                }

            }
            catch { }

            if (longestmatch.Length < 10) return source; //not worth it

            string result = source.Replace(longestmatch, longestmatch.truncateTo(targetlength) + " ");

            return result;
        }
        */
    }
}
