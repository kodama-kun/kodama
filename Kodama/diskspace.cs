﻿using System;

namespace Kodama
{
    class DiskSpace
    {

        private static DateTime lastDiskSpaceCheck = new DateTime(1970,1,1);
        private static int lastDiskSpace;
        /// <summary>
        /// returns the available disk space in gigabytes.
        /// </summary>
        /// <returns></returns>
        public static int GetDiskSpace()
        {
            if ((DateTime.Now - lastDiskSpaceCheck).TotalMinutes < 10) return lastDiskSpace;

            lastDiskSpaceCheck = DateTime.Now;

            try
            {
                System.IO.DriveInfo c = new System.IO.DriveInfo(FileTools.GetDriveRoot(Settings.All.CompletedPath));
                long cAvailableSpace = c.AvailableFreeSpace / (1024 * 1024 * 1024);
                lastDiskSpace = (int)cAvailableSpace;
                return lastDiskSpace;
            }
            catch
            {

            }
            return -1;
        }
    }
}
