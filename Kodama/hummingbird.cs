﻿using System;
using System.Collections.Generic;
//using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace Kodama
{
    class Hummingbird
    {
        public static EmbeddedDatabase edb;

        static string dbpath = "charts.sdf";

        public static void Startup()
        {
            edb = new EmbeddedDatabase(dbpath);
            if (System.IO.File.Exists(dbpath) == false) initDB();
        }


        public static void parseSinglePage(int pagenum)
        {
            Debug.WriteLine("Parsing page " + pagenum);
            var urls = loadList("https://hummingbird.me/anime/filter/oldest?page=" + pagenum + "&y[]=Upcoming&y[]=2010s&y[]=2000s&y[]=1990s&y[]=1980s&y[]=1970s&y[]=Older"); // /anime/hanawa-hekonai-kappa-matsuri
            if (urls.Count == 0) return;


            foreach (var url in urls)
            {
                if (edb.IsInDB("anime", "url", url))
                {
                    Debug.WriteLine("Already in DB");
                    continue;
                } //should update the entry maybe?

                Debug.WriteLine("Adding to DB...");
                try { insertAnime(loadSingle("https://hummingbird.me/anime/" + url), url); }
                catch
                {
                    insertAnime(loadSingle("https://hummingbird.me/anime/" + url), url);
                }

            }
        }


        public static int getLastPage()
        {
            string rawpage = Tools.GetRawHTML("https://hummingbird.me/anime/filter/oldest?page=1&y[]=Upcoming&y[]=2010s&y[]=2000s&y[]=1990s&y[]=1980s&y[]=1970s&y[]=Older");
            var rawblob = Tools.SingleRegex(rawpage, "Next &rsaquo;", "Last &raquo;");
            var pageblob = Tools.SingleRegex(rawblob, "page=", "&");
            try { return Int32.Parse(pageblob); }
            catch { }
            return 300;
        }



        public static void insertAnime(jsonFragment.FullAnime source, string siteurl)
        {


            string genrestring = "";
            if (source.genres != null)
            {
                foreach (string str in source.genres)
                {
                    genrestring += str + "|";
                }
                genrestring = genrestring.TrimEnd('|');
            }
            string dts = null;// "1900-01-01";
            try
            {
                var dt = DateTime.Parse(source.started_airing);
                //  log(dt);
                dts = dt.ToString("yyyy-MM-dd");
            }
            catch { return; /*don't insert shows without a date*/  }

            string franch = "";
            if (source.franchise_ids != null)
            {
                foreach (string str in source.franchise_ids)
                {
                    franch += str + "|";
                }
                franch = franch.TrimEnd('|');
            }


            int epcount = 0;
            if (source.episode_count != null) epcount = (int)source.episode_count;
            float rating = 0f;
            if (source.bayesian_rating != null) rating = (float)source.bayesian_rating;

            string romaji = null;
            if (source.romaji_title != source.canonical_title) romaji = source.romaji_title;

            string alternate = null;
            if (source.alternate_title != source.english_title) alternate = source.alternate_title;


            edb.Insert("anime",
                source.canonical_title,
                source.english_title,
                romaji,
                alternate,
                source.synopsis,
                source.poster_image,
                source.show_type,
                dts,// "1947/08/15 03:33:20",//  source.started_airing,
                source.episode_count,
                source.bayesian_rating,
                genrestring,
                franch,
                siteurl);
        }




        public static void initDB()
        {
            edb.ResetDB();
            edb.CreateTable("anime", "canonical_title nvarchar(500) NOT NULL",
                "english_title nvarchar(500) NULL",
                "romaji_title nvarchar(500) NULL",
                "alternate_title nvarchar(500) NULL",
                "synopsis nvarchar(4000) NULL",
                "poster_image nvarchar(500) NULL",
                "show_type nvarchar(500) NULL",
                "started_airing  datetime NULL",
                "episode_count integer NULL",
                //     "finished_airing nvarchar(500)",
                //     "cover_image nvarchar(500) NULL",
                "bayesian_rating float NULL",
                "genres nvarchar(500) NULL",
                "franchise nvarchar(500) NULL",
                "url nvarchar(500) NOT NULL");

            edb.CreateIndex("anime", "myindex", "url");
        }


        public static jsonFragment.FullAnime loadSingle(string urlsource)
        {
            //   string rawpage = getRawHTML("https://hummingbird.me/anime/mikagura-gakuen-kumikyoku");

            string rawpage = Tools.GetRawHTML(urlsource);

            var bigrawblob = Tools.SingleRegex(rawpage, "window.preloadData = ([\\d|\\D]*?)window.genericPreload");

            try
            {
                bigrawblob = bigrawblob.Substring(bigrawblob.IndexOf('[') + 1);
                bigrawblob = bigrawblob.Substring(0, bigrawblob.LastIndexOf(']'));
            }
            catch
            {
                System.Diagnostics.Debug.WriteLine(rawpage);
                throw (new Exception());
            }

            try
            {

                using (MemoryStream stream1 = new MemoryStream(Encoding.UTF8.GetBytes(bigrawblob ?? "")))
                {
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(jsonFragment.RootObject));
                    stream1.Position = 0;
                    jsonFragment.RootObject rootobj = (jsonFragment.RootObject)ser.ReadObject(stream1);

                    if (rootobj.full_anime.Count > 1) Debug.WriteLine("multiple anime in json!!!!");
                    return rootobj.full_anime[0];
                }

                /*
                var rootobj = Newtonsoft.Json.JsonConvert.DeserializeObject<jsonFragment.RootObject>(bigrawblob);

                if (rootobj.full_anime.Count > 1) Debug.WriteLine("multiple anime in json!!!!");

                return rootobj.full_anime[0];*/

            }
            catch (Exception e) { Debug.WriteLine(e); }

            return null;
        }



        public static List<string> loadList(string listurl)
        {
            List<string> result = new List<string>();

            //   string rawpage = getRawHTML("https://hummingbird.me/anime/filter/oldest?page=1&y[]=Upcoming&y[]=2010s&y[]=2000s&y[]=1990s&y[]=1980s&y[]=1970s&y[]=Older");
            try
            {


                string rawpage = Tools.GetRawHTML(listurl);

              //  var pagedrops = tools.multiRegex(rawpage, "large-2 columns single-wrapper([\\d|\\D]*?)p class='title'");

                var pagedrops = Tools.MultiRegex(rawpage, "large-2 columns single-wrapper", "p class='title'");

                foreach (var rdrop in pagedrops)
                {
                  //  var name = tools.singleRegex(rdrop, "data-tooltip title='([\\d|\\D]*?)'>");
                    var name = Tools.SingleRegex(rdrop, "data-tooltip title='", "'>");
                   // var urlfix = tools.singleRegex(rdrop, "<a href=\"/anime/([\\d|\\D]*?)\"");
                    var urlfix = Tools.SingleRegex(rdrop, "<a href=\"/anime/", "\"");
                    // textBox1.Text += name + " " + urlfix + "\r\n";

                    result.Add(urlfix);
                }
            }
            catch (Exception ex) { Debug.WriteLine(ex); }
            return result;
        }

    }





    public class jsonFragment
    {
        /* public class Quote
         {
             public int id { get; set; }
             public string anime_id { get; set; }
             public string character_name { get; set; }
             public string content { get; set; }
             public string username { get; set; }
             public int favorite_count { get; set; }
             public object is_favorite { get; set; }
         }

         public class Review
         {
             public int id { get; set; }
             public string summary { get; set; }
             public int positive_votes { get; set; }
             public int total_votes { get; set; }
             public double rating { get; set; }
             public double rating_story { get; set; }
             public double rating_animation { get; set; }
             public double rating_sound { get; set; }
             public double rating_characters { get; set; }
             public double rating_enjoyment { get; set; }
             public string content { get; set; }
             public string formatted_content { get; set; }
             public object liked { get; set; }
             public string user_id { get; set; }
             public string anime_id { get; set; }
         }

         public class User
         {
             public string id { get; set; }
             public string cover_image_url { get; set; }
             public string avatar_template { get; set; }
             public string rating_type { get; set; }
             public string bio { get; set; }
             public string about { get; set; }
             public bool is_followed { get; set; }
             public string location { get; set; }
             public string website { get; set; }
             public string waifu { get; set; }
             public string waifu_or_husbando { get; set; }
             public string waifu_slug { get; set; }
             public string waifu_char_id { get; set; }
             public object last_sign_in_at { get; set; }
             public object current_sign_in_at { get; set; }
             public bool is_admin { get; set; }
             public int following_count { get; set; }
             public int follower_count { get; set; }
             public object is_pro { get; set; }
             public object about_formatted { get; set; }
         }

         public class Casting
         {
             public int id { get; set; }
             public string role { get; set; }
             public string language { get; set; }
             public int person_id { get; set; }
             public int character_id { get; set; }
         }

         public class Person
         {
             public int id { get; set; }
             public string name { get; set; }
             public string image { get; set; }
         }

         public class Character
         {
             public int id { get; set; }
             public string name { get; set; }
             public string image { get; set; }
         }

         public class Producer
         {
             public string id { get; set; }
             public string name { get; set; }
         }
         */
        public class Episode
        {
            public int id { get; set; }
            public int number { get; set; }
            public string title { get; set; }
            public string thumbnail { get; set; }
            public object synopsis { get; set; }
            public string anime_id { get; set; }
        }

        public class FullAnime
        {
            public string id { get; set; }
            public string canonical_title { get; set; }
            public string english_title { get; set; }
            public string romaji_title { get; set; }
            public string alternate_title { get; set; }
            public string synopsis { get; set; }
            public string poster_image { get; set; }
            public string show_type { get; set; }
            public string started_airing { get; set; }
            public int? episode_count { get; set; }
            public string finished_airing { get; set; }
            public string cover_image { get; set; }
            public bool? started_airing_date_known { get; set; }
            public double? bayesian_rating { get; set; }
            public List<string> genres { get; set; }
            public List<string> franchise_ids { get; set; }





            /*  public object pending_edits { get; set; }
                   //    public string updated_at { get; set; }         
          public List<int> community_ratings { get; set; }
       //     public List<object> screencaps { get; set; }
       //     public List<string> languages { get; set; }
         public int episode_length { get; set; }
        //    public string youtube_video_id { get; set; }
              public object has_reviewed { get; set; }
                        public string age_rating { get; set; }
               public int cover_image_top_offset { get; set; }
            public string age_rating_guide { get; set; }
              public List<int> featured_quote_ids { get; set; }
              public List<int> trending_review_ids { get; set; }
              public List<int> featured_casting_ids { get; set; }
              public List<string> producer_ids { get; set; }
              
              public List<int> episode_ids { get; set; }*/
            //    public object library_entry_id { get; set; }
        }

        public class RootObject
        {
            /*     public List<Quote> quotes { get; set; }
                 public List<Review> reviews { get; set; }
                 public List<User> users { get; set; }
                 public List<Casting> castings { get; set; }
                 public List<Person> people { get; set; }
                 public List<Character> characters { get; set; }
                 public List<Producer> producers { get; set; }*/
            public List<Episode> episodes { get; set; }
            public List<object> library_entries { get; set; }
            public List<FullAnime> full_anime { get; set; }
        }


    }
}
