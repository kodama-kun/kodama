﻿using System;
using System.Threading.Tasks;

using System.Data;
using System.Data.SqlServerCe;
using System.IO;

namespace Kodama
{

    public class EmbeddedDatabase
    {
        string filename;
        string connStr;// = "Data Source = Test.sdf; Password = <password>";


        public EmbeddedDatabase(string filename)
        {
            this.filename = filename;
            this.connStr = "Data Source = "+filename+"; Password = <password>";
        }

        public void ResetDB()
        {
            if (File.Exists(filename)) File.Delete(filename);

            SqlCeEngine engine = new SqlCeEngine(connStr);
            engine.CreateDatabase();
            engine.Dispose();
        }

        public void CreateTable(string tname, params string[] columns)
        {
            using (SqlCeConnection conn = new SqlCeConnection(connStr))
            {
                conn.Open();

                //max size for nvarchar is 4000
                SqlCeCommand cmd = conn.CreateCommand();//https://technet.microsoft.com/en-us/library/ms172424%28v=sql.110%29.aspx
                cmd.CommandText = "CREATE TABLE " + tname + " (";

                foreach (string str in columns)
                {
                    cmd.CommandText += str + ",";
                }
                cmd.CommandText = cmd.CommandText.TrimEnd(',');
                cmd.CommandText += ")";
                cmd.ExecuteNonQuery();
            }
        }



        public int GetTableCount(string tname)//, string scmd, params object[] cmdparams)
        {
            using (SqlCeConnection conn = new SqlCeConnection(connStr))
            {
                conn.Open();

                SqlCeCommand cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT COUNT (*) FROM "+tname;

                try
                {
                    int result = (int)cmd.ExecuteScalar();
                    Debug.WriteLine("Tablecount: " + result);
                    return result;
                }
                catch (Exception ex) { Debug.WriteLine("Failed TableCount: " + ex); return -1; }
            }

        }

       
        public DataTable DisplayTable(string tname, string columns)
        {
            DataTable dt=null;
            using (SqlCeConnection conn = new SqlCeConnection(connStr))
            {
                conn.Open();

                var cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT "+columns+" FROM " + tname;
                using (var reader = cmd.ExecuteReader())
                {

                    DataSet ds = new DataSet();
                    dt = new DataTable(tname);
                    ds.Tables.Add(dt);
                    ds.Load(reader, LoadOption.PreserveChanges, ds.Tables[0]);
                   

                    reader.Close();
                }
            }
            return dt;
        }

        public DataTable DisplayTableDateBetween(string tname, string columns, string datecolumn, DateTime start, DateTime end)
        {
            DataTable dt = null;
            using (SqlCeConnection conn = new SqlCeConnection(connStr))
            {
                conn.Open();

                var cmd = conn.CreateCommand();
                cmd.CommandText = "SELECT " + columns + " FROM " + tname +" WHERE "+datecolumn+" >= @p1 AND "+datecolumn+" < @p2";
                cmd.Parameters.Add("@p1", start);
                cmd.Parameters.Add("@p2", end);

                using (var reader = cmd.ExecuteReader())
                {

                    DataSet ds = new DataSet();
                    dt = new DataTable(tname);
                    ds.Tables.Add(dt);
                    ds.Load(reader, LoadOption.PreserveChanges, ds.Tables[0]);


                    reader.Close();
                }
            }
            return dt;
        }



        public DataTable DisplayTableCMD(string tname, string scmd, params object[] cmdparams)
        {
            DataTable dt = null;
            using (SqlCeConnection conn = new SqlCeConnection(connStr))
            {
                conn.Open();

                var cmd = conn.CreateCommand();
                cmd.CommandText = scmd;

                for (int i = 0; i < cmdparams.Length;i++ )
                {
                    cmd.Parameters.Add("@p" + (i+1).ToString(), cmdparams[i]);
                }
                
                using (var reader = cmd.ExecuteReader())
                {

                    DataSet ds = new DataSet();
                    dt = new DataTable(tname);
                    ds.Tables.Add(dt);
                    ds.Load(reader, LoadOption.PreserveChanges, ds.Tables[0]);


                    reader.Close();
                }
            }
            return dt;
        }


        public async Task<DataTable> DisplayTableCMDAsync(string tname, string scmd, params object[] cmdparams)
        {
            DataTable dt = null;
            using (SqlCeConnection conn = new SqlCeConnection(connStr))
            {
                conn.Open();

                var cmd = conn.CreateCommand();
                cmd.CommandText = scmd;

                for (int i = 0; i < cmdparams.Length; i++)
                {
                    cmd.Parameters.Add("@p" + (i + 1).ToString(), cmdparams[i]);
                }

                using (var reader = cmd.ExecuteReaderAsync())
                {

                    DataSet ds = new DataSet();
                    dt = new DataTable(tname);
                    ds.Tables.Add(dt);
                    ds.Load(await reader, LoadOption.PreserveChanges, ds.Tables[0]);
                   // reader.close();
                }
            }
            return dt;
        }

       

        public void Insert(string tname, params object[] fields)
        {
            using (SqlCeConnection conn = new SqlCeConnection(connStr))
            {
                conn.Open();
                var cmd = conn.CreateCommand();

                //      System.Diagnostics.Debug.WriteLine("Fields: " + fields.Length);

                cmd.CommandText = "INSERT INTO " + tname + " VALUES (";

                for (int i = 0; i < fields.Length; i++)
                {
                    cmd.CommandText += "@p" + i + ",";
                }
                cmd.CommandText = cmd.CommandText.TrimEnd(',');
                cmd.CommandText += ")";

                for (int i = 0; i < fields.Length; i++)
                {
                    if (fields[i] != null) { cmd.Parameters.Add("@p" + i, fields[i]); }
                    else
                    {
                        cmd.Parameters.Add("@p" + i, DBNull.Value);
                        //  System.Diagnostics.Debug.WriteLine("Param #" + i + " is null");
                    }
                }

                cmd.ExecuteNonQuery();
            }
        }

        public bool IsInDB(string tname, string key, string value)
        {
            using (SqlCeConnection conn = new SqlCeConnection(connStr))
            {
                conn.Open();
                var cmd = conn.CreateCommand();

                cmd.CommandText = "SELECT COUNT(*) from " + tname + " WHERE " + key + "='" + value + "'";
                int count = (int)cmd.ExecuteScalar();

                if (count > 0) return true;
            }

            return false;
        }



        public void CreateIndex(string tname, string indexname, string columnname)
        {
            using (SqlCeConnection conn = new SqlCeConnection(connStr))
            {
                conn.Open();
                var cmd = conn.CreateCommand();


                cmd.CommandText = "CREATE UNIQUE INDEX " + indexname + " on " + tname + " (" + columnname + ")";

                cmd.ExecuteNonQuery();
            }
        }


    }

}
