﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
//using System.Diagnostics;
using System.Windows.Threading;

namespace Kodama
{

    public class UIHelper
    {
        public static Dispatcher uiDispatcher;

        public static ObservableCollection<ChartView> _FooList = new ObservableCollection<ChartView>();
        public static ObservableCollection<ChartView> FooList
        {
            get { return _FooList; }
            set { _FooList = value; }
        }


        public static ObservableCollection<NyaaTab> _TabList = new ObservableCollection<NyaaTab>();
        public static ObservableCollection<NyaaTab> TabList
        {
            get { return _TabList; }
            set { _TabList = value; }
        }


        private static ObservableCollection<Folder> _FolderList = new ObservableCollection<Folder>();
        public static ObservableCollection<Folder> FolderList
        {
            get { return _FolderList; }
            set { _FolderList = value; }
        }

        private static ObservableCollection<FranchiseButton> _FranchiseList = new ObservableCollection<FranchiseButton>();
        public static ObservableCollection<FranchiseButton> FranchiseList
        {
            get { return _FranchiseList; }
            set { _FranchiseList = value; }
        }


        private static SettingsHelperThing _SettingsHelper = new SettingsHelperThing();
        public static SettingsHelperThing SettingsHelper
        {
            get { return _SettingsHelper; }
            set { _SettingsHelper = value; }
        }
    }



    public class SettingsHelperThing
    {
        public int ListenPort { get { return Settings.All.ListenPort; } set { Settings.All.ListenPort = value; } }
        public int ConnectionDownSpeed { get { return Settings.All.ConnectionDownSpeed; } set { Settings.All.ConnectionDownSpeed = value; } }
        public int ConnectionUpSpeed { get { return Settings.All.ConnectionUpSpeed; } set { Settings.All.ConnectionUpSpeed = value; } }

        public string ReleaseGroups
        {
            get
            {
                string rgs = "";
                foreach (var s in Settings.All.ReleaseGroups)
                {
                    rgs += s + "\n";
                }
                rgs = rgs.TrimEnd('\n');
                return rgs;
            }
            set
            {
                var templist = new List<string>();

                foreach (var str in value.Split('\n'))
                {
                    var str2 = str.Trim();
                    if (str2.Length > 0) templist.Add(str2);
                }
                Settings.All.ReleaseGroups = templist.ToArray();

            }
        }
    }






    public class ChartView
    {
        public string Name { get; set; }
        public ObservableCollection<ChartEntry> ChartEntryList { get; set; }
    }

    public class ChartEntry
    {
        public string aid { get; set; }
        public string _url { get; set; }
        public string Name { get; set; }
    }










    public class Folder
    {
        public Folder()
        {
            Folders = new ObservableCollection<Folder>();
        }

        //if no children -> selectable!

        public DateTime start, end;
        public string Label { get; set; }
        public ObservableCollection<Folder> Folders { get; set; }

    }



    public class FranchiseButton
    {
        public string Label { get; set; }
        public string Url { get; set; }
    }




}
