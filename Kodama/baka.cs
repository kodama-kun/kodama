﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
//using System.Diagnostics;
using System.Net;
using System.Data;

namespace Kodama
{
    class BakaDB
    {

        public static BackgroundWorker worker;

        public static EmbeddedDatabase edb;

        static string dbpath = "baka.sdf";

        public static void Startup()
        {
            edb = new EmbeddedDatabase(dbpath);
            if (System.IO.File.Exists(dbpath) == false) initDB();

            worker = new BackgroundWorker();
            worker.DoWork += worker_DoWork;
        }

        public static void initDB()
        {
            edb.ResetDB();
            edb.CreateTable("baka", "title nvarchar(500) NOT NULL",
                //    "torrent nvarchar(500) NULL",
                //    "stats nvarchar(500) NULL",
                "size nvarchar(500) NULL",
                "cat nvarchar(500) NULL",
                "tags nvarchar(500) NULL",
                "url nvarchar(500) NOT NULL");


            edb.CreateIndex("baka", "myindex", "url");
        }

        public static void StartGeneratingDB()
        {
            worker.RunWorkerAsync();
        }

        private static void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            GenerateDB();
        }

        private static void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }


        /*	bakaAllowedCategories = new configer("Anime Movie|Anime Series|OVA", "bakaAllowedCategories", null);
			bakaRxBlocks          = new configer("td class=\"category([\\d|\\D]*?)/tr", "bakaRxBlocks", null);
			bakaRxTitle           = new configer("Download torrent: ([\\d|\\D]*?)\">", "bakaRxTitle", null);
			bakaRxUrl             = new configer("class=\"title\" href=\"([\\d|\\D]*?)\"", "bakaRxUrl", null);
			bakaRxCat             = new configer("browse.php\\?cat[\\d|\\D]*?alt=\"([\\d|\\D]*?)\"", "bakaRxCat", null);
			bakaRxSize            = new configer("class=\"size\">([\\d|\\D]*?)<", "bakaRxSize", null);
			bakaRxTagsBlock       = new configer("span class=\"tags\">([\\d|\\D]*?)</span", "bakaRxTagsBlock", null);
			bakaRxTag             = new configer("\">([\\d|\\D]*?)</a", "bakaRxTag", null);
			bakaRxTorrent         = new configer("download_link[\\d|\\D]*?href=\"([\\d|\\D]*?)\"", "bakaRxTorrent", null);
			bakaRxStats           = new configer("<td>Peers:[\\d|\\D]*?<td>([\\d|\\D]*?)</td>", "bakaRxStats", null);*/


        private static void GenerateDB()
        {
            List<BakaThing> allbakas = new List<BakaThing>();

            string basequery = "https://bakabt.me/browse.php?limit=100&page=";
            int idx = 0;
            int finalpage = 2000;

            List<string> results;
            do
            {
                var rawpage = Tools.GetRawHTML(basequery + idx.ToString());

                if (idx == 0)
                {
                 //   var spagenum = Tools.SingleRegex(rawpage, System.Text.RegularExpressions.Regex.Escape("hellip; | <a href=\"browse.php?limit=100&amp;page="), "\"class=");
                    var spagenum = Tools.SingleRegex(rawpage, "hellip; \\| <a href=\\\"browse\\.php\\?limit=100&amp;page=([\\d|\\D]*?)\\\" class="); //hellip; \| <a href=\"browse\.php\?limit=100&amp;page=([\d|\D]*?)\"class=
                    finalpage = Int32.Parse(spagenum)+1;
                }

                worker.ReportProgress((int)(((double)idx / (double)finalpage) * 100.0));

                idx++;

                results = Tools.MultiRegex(rawpage, "<tr([\\d|\\D]*?)\\/tr>"); //<tr([\d|\D]*?)\/tr>
            //    results = GetTableBlocks(rawpage);

                foreach (var resultblock in results)
                {
                    try
                    {
                        var bthing = parseSingleBlock(resultblock);
                        if (bthing.title != "" && bthing.url != "")
                        {
                            insertBakaThing(bthing);
                        }
                        else 
                        {
                            Debug.WriteLine("Ignored block: " + resultblock);
                        }
                    }
                    catch (Exception ex) { Debug.WriteLine(ex); }
                }
            //   break;
            } while (results.Count != 0 && idx <= finalpage);
        }


    /*   static  List<string> GetTableBlocks(string source)
        {
            var rawblocks = source.Split(new string[] { "<tr" }, StringSplitOptions.None).ToList<string>();
            rawblocks.RemoveAt(0); // Trim the start
            rawblocks = Sieve(rawblocks, "torrent");
            return rawblocks;
        }

       static List<string> Sieve(List<string> source, string mustContain)
       {
           var result = new List<string>();

           foreach (string str in source)
           {
               if (str.Contains(mustContain)) result.Add(str);
           }

           return result;
       }
        */
        static BakaThing parseSingleBlock(string source)
        {
            BakaThing result = new BakaThing();

         //   Tools.Log("\n"+source);

            //https://regex101.com/
            //http://www.freeformatter.com/java-dotnet-escape.html#ad-output

            string title = Tools.SingleRegex(source, "Download torrent: ([\\d|\\D]*?)\\\">");

            if (title == "") 
            {
                title = Tools.SingleRegex(source, "<a[\\d|\\D]*?>([\\d|\\D]*?)<");

            }

            string url = Tools.SingleRegex(source, "href=\\\"torrent([\\d|\\D]*?)\\\"");
            string cat = Tools.SingleRegex(source, "title=\\\"([\\d|\\D]*?)\\\" class=\\\"icon");
            string tagsblock = Tools.SingleRegex(source, "class=\\\"tags\\\">([\\d|\\D]*?)<\\/span");
            string size = Tools.SingleRegex(source, "class=\\\" size\\\">([\\d|\\D]*?)<");



            Debug.WriteLine(title + " " + url + " " + cat + " " + size);

            string tags = "";
            List<string> taglist = Tools.MultiRegex(tagsblock, "\">", "</a");
            foreach (string str in taglist) tags += str.Replace("|", "") + "|";
            tags = tags.Trim('|');



            
            result.title = WebUtility.HtmlDecode(title);

            result.url = url;
            result.cat = cat;
            result.tags = tags;
            result.size = size;
            //    result.torrent = torrent;
            //    result.stats = stats;

            return result;
        }

        public static string GetTorrentFromUrl(string url)
        {
            string rawdata = Tools.GetRawHTML("https://bakabt.me/torrent" + url);


            string torrent = Tools.SingleRegex(rawdata, "download_link[\\d|\\D]*?href=\"", "\"");
            string stats = Tools.SingleRegex(rawdata, "<td>Peers:[\\d|\\D]*?<td>", "</td>");

            Debug.WriteLine(torrent + " " + stats);

            return torrent;
        }

        static void insertBakaThing(BakaThing source)
        {
            try
            {
                edb.Insert("baka",

                       source.title,
                    //    source.torrent,
                    //     source.stats,
                       source.size,
                       source.cat,
                       source.tags,
                       source.url
                       );
            }
            catch (Exception ex) { Debug.WriteLine(ex); }
        }
    }





    public class BakaSearch
    {
        private static Dictionary<string, List<BakaThing>> TitleLookup;
        private static Object thisLock = new Object();


        public static BackgroundWorker cacheWorker = new BackgroundWorker();

        public static void RebuildTitleCache()
        {
            cacheWorker.DoWork += RebuildTitleCacheWorker;
            cacheWorker.RunWorkerAsync();
        }

        private static void RebuildTitleCacheWorker(object sender, DoWorkEventArgs e)
        {
            lock (thisLock)
            {
                if (TitleLookup == null)
                {
                    TitleLookup = new Dictionary<string, List<BakaThing>>();
                    var bakatable = BakaDB.edb.DisplayTable("baka", "*");

                    for (int i = 0; i < bakatable.Rows.Count; i++)
                    {
                        var fullrawtitle =  bakatable.Rows[i].Field<String>(0);
                        var titles = fullrawtitle.Split('|');
                        string torrent = "";
                        string cat = "";
                        try
                        {
                            torrent = bakatable.Rows[i].Field<String>(4);
                            cat = bakatable.Rows[i].Field<String>(2);
                        }
                        catch { }

                        foreach (var rawtitle in titles)
                        {
                            try
                            {
                                string title = rawtitle;

                             //   Tools.Log(title);


                                var brackets = SearchNyaa.NyaaTools.GetBrackets(title);
                                foreach (string str in brackets)
                                {
                                    title = title.Replace(str, "");
                                }
                                title = title.Trim();

                                if (TitleLookup.ContainsKey(title) == false)
                                {
                                    TitleLookup.Add(title, new List<BakaThing>());
                                }

                                var bakathing = new BakaThing();
                                bakathing.title = fullrawtitle.Trim();
                                bakathing.cat = cat;
                                bakathing.url = torrent;
                                

                                TitleLookup[title].Add(bakathing);

                                // TitleLookup.Add(title, new Tuple<string, string, string>(title, torrent));
                            }
                            catch { } //some torrents have the same title
                        }
                    }
                }
            }
        }


        public static SearchNyaa.NyaaItem[] SearchForTitle(string query, int maxResults)
        {
            Debug.WriteLine("Waiting for lock: " + query);
            lock (thisLock)
            {

            }

            Debug.WriteLine("Lock finished: " + query);
            var results = new List<Tuple<int, SearchNyaa.NyaaItem>>();

            foreach (var testtitle in TitleLookup.Keys)
            {

                int currentDistance = Tools.LevenshteinDistance(query, testtitle);

                if (currentDistance < 5)
                {
                    foreach (var bakathing in TitleLookup[testtitle])
                    {
                        string torrent = bakathing.url;
                        try
                        {
                            torrent = "https://bakabt.me/" + BakaDB.GetTorrentFromUrl(torrent);
                        }
                        catch { }

                        string pluscat = "";
                        if(bakathing.cat!="") pluscat = " (" + bakathing.cat + ")";
                        results.Add(new Tuple<int, SearchNyaa.NyaaItem>(currentDistance, new SearchNyaa.NyaaItem(bakathing.title + pluscat, testtitle, testtitle, torrent)));
                    }

                    results.Sort((x, y) => x.Item1.CompareTo(y.Item1));

                    if (results.Count > maxResults) results.RemoveAt(results.Count - 1);
                }
            }

            var result = new SearchNyaa.NyaaItem[results.Count];
            for (int i = 0; i < results.Count; i++) result[i] = results[i].Item2;
            return result;
        }
    }




    public class BakaThing
    {
        public string title;
        //    public string torrent;
        //    public string stats;
        public string size;
        public string cat;
        public string tags;
        public string url;
    }





}
