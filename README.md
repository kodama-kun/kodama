# Kodama #

- Browse anime series charts from the Hummingbird database.
- Search and download from Nyaa.se and BakaBT.
- Add shows to your watchlist and have them download automatically.


### DOWNLOAD THE PROGRAM ###

[Download link](https://bitbucket.org/kodama-kun/kodama/downloads/Kodama.zip)


### WHAT TO DO FIRST ###
### 1. Go to Charts tab, click "Charts DB" in the lower left, click Update Database. ###
### 2. Go to Search tab, click "Baka DB" in the lower right, click Update Database. ###
### 3. Wait until both are done. This might take >30 minutes. ###

Now you can browse charts and search for series from Baka.

![downloads.png](https://bitbucket.org/repo/kzLzAj/images/3773098217-downloads.png)
![charts.png](https://bitbucket.org/repo/kzLzAj/images/4261554088-charts.png)
![search.png](https://bitbucket.org/repo/kzLzAj/images/3811367688-search.png)
![settings.png](https://bitbucket.org/repo/kzLzAj/images/3504562760-settings.png)
### How do I compile the sourcecode? ###

Requires Microsoft SQL Server Compact Edition https://www.nuget.org/packages/Microsoft.SqlServer.Compact
This project should compile with any recent version of Visual Studio.


### Legal ###

Released under WTFPL

Do whatever you want with this. If you're going to use it to build nuclear missiles, be my guest.

Includes parts of MonoTorrent library (MIT License) by Alan McGovern https://github.com/mono/monotorrent